﻿Imports System.Data.SqlClient
Imports System.IO
Imports JV_Fonctions



Public Class Form1
    Dim refArt As String
    Dim libArt As String
    Dim refExist As Boolean
    Dim libExist As Boolean

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'cacher l'option de filtrage par date à l'ouverture de l'application et la generation du fichier Excel
        OptionnelGroupBox.Hide()
        ExcelGroupBox.Hide()
        CheckExcel.Hide()

        
    End Sub

    Private Sub FiltreDate_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FiltreDate.CheckedChanged
        If FiltreDate.Checked = True Then
            OptionnelGroupBox.Show()
        Else
            OptionnelGroupBox.Hide()
        End If
    End Sub

    Private Sub GetData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GetData.Click
        ArtError.Text = ""

        Dim cnx As New SqlConnection
        Dim cmd As New SqlCommand

        Dim req As String
        Dim dataCustomers As New DataTable

        Dim requeteComplete As String = ""
        Dim datefrom As Date = Today
        Dim dateto As Date = Today


        datefrom = FromDate.Text
        dateto = ToDate.Text
        refArt = UCase(ArtRef.Text)
        libArt = ArtLib.Text
        refExist = ArtRef_Valid()
        libExist = ArtLib_Valid()

        '*************************DEBUT CONTROLE*************************'
        If refExist = True Or libExist = True Then
            Try

                If refArt <> "" And libArt = "" Then
                    'recuperer la requete qui recherche via la référence
                    req = RequetSQLi.requetSQLRef
                    requeteComplete = String.Format(req, refArt)
                End If

                If refArt <> "" And libArt = "" Then
                    'recuperer la requete qui recherche via la référence + dates
                    req = RequetSQLi.requetSQLRefDates
                    requeteComplete = String.Format(req, refArt, datefrom, dateto)
                End If


                If libArt <> "" And refArt = "" Then
                    'recuperer la requete qui recherche via le libelle
                    req = RequetSQLi.requetSQLLib
                    requeteComplete = String.Format(req, libArt)
                End If

                If libArt <> "" And refArt = "" Then
                    'recuperer la requete qui recherche via la libelle + dates
                    req = RequetSQLi.requetSQLLibDates
                    requeteComplete = String.Format(req, libArt, datefrom, dateto)
                End If

                '****************************************************************'


                'connexion à la BDD
                cnx.ConnectionString = "Data Source=SRV-SQL\SAGE100; initial catalog=ABYSSE; Integrated Security=True"
                cnx.Open()

                If requeteComplete <> "" Then

                    'executer la requete
                    cmd.Connection = cnx
                    cmd.CommandText = requeteComplete

                    'lecture des données
                    Dim dreader As SqlDataReader
                    dreader = cmd.ExecuteReader

                    'ecrire les données dans une DataTable
                    dataCustomers.Load(dreader)
                    dreader.Close()

                    'binder les données avec la DataGridView
                    DataGridView1.DataSource = dataCustomers
                    If DataGridView1.RowCount > 1 Then
                        CheckExcel.Show()
                        GetData.Hide()
                        Reload.Show()
                        ArtError.Text = "Chargement avec Succés"
                        ArtError.ForeColor = Color.Green
                    Else
                        GetData.Show()
                        CheckExcel.Hide()
                        ArtError.Text = "aucun article n'a été trouvé pour cette période"
                        ArtError.ForeColor = Color.Red

                    End If

                    'fermer la connextion
                    cnx.Close()



                End If

            Catch ex As Exception
                ArtError.Text = ex.Message
                FJV.FichierErreur(ex.Message(), , )
            End Try
        End If

    End Sub

    Private Sub DATAGRIDVIEW_TO_EXCEL(ByVal DGV As DataGridView)

        Dim path As String = Application.StartupPath
        Dim directoryData As String = path + "\Data\"
        Dim xmlFile As String = "Article"
        If refArt <> "" Then
            xmlFile = directoryData + refArt + ".xml"
        ElseIf libArt <> "" Then
            xmlFile = directoryData + refArt + ".xml"
        End If

        If (Not Directory.Exists(directoryData)) Then
            System.IO.Directory.CreateDirectory(directoryData)
        End If

        Try
            Dim DTB = New DataTable, RWS As Integer, CLS As Integer

            For CLS = 0 To DGV.ColumnCount - 1  ' COLUMNS OF DTB
                DTB.Columns.Add(DGV.Columns(CLS).Name.ToString)
            Next

            Dim DRW As DataRow

            For RWS = 0 To DGV.Rows.Count - 1 ' FILL DTB WITH DATAGRIDVIEW
                DRW = DTB.NewRow

                For CLS = 0 To DGV.ColumnCount - 1
                    Try
                        If DGV.Rows(RWS).Cells(CLS).Value.ToString <> "preserve" Then
                            DRW(DTB.Columns(CLS).ColumnName.ToString) = DGV.Rows(RWS).Cells(CLS).Value.ToString
                        End If
                    Catch ex As Exception
                        FJV.FichierErreur(ex.Message(), , )
                    End Try
                Next
                DTB.Rows.Add(DRW)
            Next

            DTB.AcceptChanges()

            Dim DST As New DataSet
            DST.Tables.Add(DTB)
            Dim FLE As String = xmlFile ' PATH AND FILE NAME WHERE THE XML WIL BE CREATED (EXEMPLE: C:\REPS\XML.xml)
            DTB.WriteXml(FLE)
            Dim EXL As String = "C:\Program Files (x86)\Microsoft Office\Office16\EXCEL.EXE" ' PATH OF/ EXCEL.EXE IN YOUR MICROSOFT OFFICE
            Shell(Chr(34) & EXL & Chr(34) & " " & Chr(34) & FLE & Chr(34), vbNormalFocus) ' OPEN XML WITH EXCEL

        Catch ex As Exception
            ExcelError.Text = ex.Message
            FJV.FichierErreur(ex.Message(), , )
        End Try

    End Sub

    Private Sub CheckExcel_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckExcel.CheckedChanged

        If CheckExcel.Checked = True Then
            ExcelGroupBox.Show()
        Else
            ExcelGroupBox.Hide()
        End If

    End Sub

    Private Sub GetExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GetExcel.Click
        DATAGRIDVIEW_TO_EXCEL((DataGridView1))
    End Sub

    Private Sub Reload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Reload.Click

        Me.Controls.Clear() 'removes all the controls on the form
        InitializeComponent() 'load all the controls again
        OptionnelGroupBox.Hide()
        ExcelGroupBox.Hide()
        CheckExcel.Hide()

    End Sub


    Private Function ArtRef_Valid()
        Dim cnx As New SqlConnection
        Dim cmd As New SqlCommand
        Dim requeteComplete As String
        Dim req As String
        Dim result As Integer

        Try
            refArt = UCase(ArtRef.Text)
            'connexion à la BDD
            cnx.ConnectionString = "Data Source=SRV-SQL\SAGE100; initial catalog=ABYSSE; Integrated Security=True"
            cnx.Open()
            cmd.Connection = cnx

            'ecriture et execution de la requete
            req = requetSQLGetAllByRef
            requeteComplete = String.Format(req, refArt)
            cmd.CommandText = requeteComplete

            'lecture des données
            Dim dreader As SqlDataReader
            dreader = cmd.ExecuteReader
            dreader.Read()
            result = dreader(0)
            dreader.Close()
            cnx.Close()

        Catch ex As Exception
            ExcelError.Text = ex.Message
            FJV.FichierErreur(ex.Message(), , )
        End Try

        If result = 0 Then
            ArtError.Text = "la syntaxe est incorrecte ou cet article est inexistant !"
            ArtError.ForeColor = Color.Red
            Return False
        Else
            Return True
        End If

    End Function


    Private Function ArtLib_Valid()
        Dim cnx As New SqlConnection
        Dim cmd As New SqlCommand
        Dim requeteComplete As String
        Dim req As String
        Dim result As Integer

        Try
            libArt = UCase(ArtLib.Text)
            'connexion à la BDD
            cnx.ConnectionString = "Data Source=SRV-SQL\SAGE100; initial catalog=ABYSSE; Integrated Security=True"
            cnx.Open()
            cmd.Connection = cnx

            'ecriture et execution de la requete
            req = requetSQLGetAllByLib
            requeteComplete = String.Format(req, libArt)
            cmd.CommandText = requeteComplete

            'lecture des données
            Dim dreader As SqlDataReader
            dreader = cmd.ExecuteReader
            dreader.Read()
            result = dreader(0)
            dreader.Close()
            cnx.Close()

        Catch ex As Exception
            ExcelError.Text = ex.Message
            FJV.FichierErreur(ex.Message(), , )
        End Try

        If result = 0 Then
            ArtError.Text = "la syntaxe est incorrecte ou cet article est inexistant !"
            ArtError.ForeColor = Color.Red
            Return False
        Else
            Return True
        End If

    End Function

End Class
