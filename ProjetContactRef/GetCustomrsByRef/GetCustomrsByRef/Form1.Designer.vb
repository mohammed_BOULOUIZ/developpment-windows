﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.RefArticle = New System.Windows.Forms.Label
        Me.ArtRef = New System.Windows.Forms.TextBox
        Me.LibArticle = New System.Windows.Forms.Label
        Me.ArtLib = New System.Windows.Forms.TextBox
        Me.GetData = New System.Windows.Forms.Button
        Me.SearchGroupBox = New System.Windows.Forms.GroupBox
        Me.Reload = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.OptionnelGroupBox = New System.Windows.Forms.GroupBox
        Me.TxtToDate = New System.Windows.Forms.Label
        Me.TxtFromDate = New System.Windows.Forms.Label
        Me.ToDate = New System.Windows.Forms.DateTimePicker
        Me.FromDate = New System.Windows.Forms.DateTimePicker
        Me.FiltreDate = New System.Windows.Forms.CheckBox
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.Ou = New System.Windows.Forms.Label
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.GetExcel = New System.Windows.Forms.Button
        Me.ExcelGroupBox = New System.Windows.Forms.GroupBox
        Me.ArtError = New System.Windows.Forms.Label
        Me.ExcelError = New System.Windows.Forms.Label
        Me.CheckExcel = New System.Windows.Forms.CheckBox
        Me.SearchGroupBox.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.OptionnelGroupBox.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ExcelGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        'RefArticle
        '
        Me.RefArticle.AutoSize = True
        Me.RefArticle.Location = New System.Drawing.Point(3, 28)
        Me.RefArticle.Name = "RefArticle"
        Me.RefArticle.Size = New System.Drawing.Size(89, 13)
        Me.RefArticle.TabIndex = 0
        Me.RefArticle.Text = "Référence Article"
        '
        'ArtRef
        '
        Me.ArtRef.Location = New System.Drawing.Point(6, 44)
        Me.ArtRef.Name = "ArtRef"
        Me.ArtRef.Size = New System.Drawing.Size(100, 20)
        Me.ArtRef.TabIndex = 1
        '
        'LibArticle
        '
        Me.LibArticle.AutoSize = True
        Me.LibArticle.Location = New System.Drawing.Point(136, 28)
        Me.LibArticle.Name = "LibArticle"
        Me.LibArticle.Size = New System.Drawing.Size(69, 13)
        Me.LibArticle.TabIndex = 2
        Me.LibArticle.Text = "Libellé Article"
        '
        'ArtLib
        '
        Me.ArtLib.Location = New System.Drawing.Point(139, 44)
        Me.ArtLib.Name = "ArtLib"
        Me.ArtLib.Size = New System.Drawing.Size(100, 20)
        Me.ArtLib.TabIndex = 3
        '
        'GetData
        '
        Me.GetData.BackColor = System.Drawing.SystemColors.GrayText
        Me.GetData.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.GetData.Location = New System.Drawing.Point(585, 22)
        Me.GetData.Name = "GetData"
        Me.GetData.Size = New System.Drawing.Size(63, 37)
        Me.GetData.TabIndex = 4
        Me.GetData.Text = "Lister"
        Me.GetData.UseVisualStyleBackColor = False
        '
        'SearchGroupBox
        '
        Me.SearchGroupBox.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.SearchGroupBox.Controls.Add(Me.Reload)
        Me.SearchGroupBox.Controls.Add(Me.GroupBox1)
        Me.SearchGroupBox.Controls.Add(Me.GroupBox3)
        Me.SearchGroupBox.Controls.Add(Me.GetData)
        Me.SearchGroupBox.Location = New System.Drawing.Point(12, 12)
        Me.SearchGroupBox.Name = "SearchGroupBox"
        Me.SearchGroupBox.Size = New System.Drawing.Size(666, 122)
        Me.SearchGroupBox.TabIndex = 5
        Me.SearchGroupBox.TabStop = False
        Me.SearchGroupBox.Text = "Critères de recherche"
        '
        'Reload
        '
        Me.Reload.Location = New System.Drawing.Point(585, 74)
        Me.Reload.Name = "Reload"
        Me.Reload.Size = New System.Drawing.Size(63, 37)
        Me.Reload.TabIndex = 15
        Me.Reload.Text = "Actualiser"
        Me.Reload.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.OptionnelGroupBox)
        Me.GroupBox1.Controls.Add(Me.FiltreDate)
        Me.GroupBox1.Location = New System.Drawing.Point(288, 14)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(291, 97)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        '
        'OptionnelGroupBox
        '
        Me.OptionnelGroupBox.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.OptionnelGroupBox.Controls.Add(Me.TxtToDate)
        Me.OptionnelGroupBox.Controls.Add(Me.TxtFromDate)
        Me.OptionnelGroupBox.Controls.Add(Me.ToDate)
        Me.OptionnelGroupBox.Controls.Add(Me.FromDate)
        Me.OptionnelGroupBox.Location = New System.Drawing.Point(113, 8)
        Me.OptionnelGroupBox.Name = "OptionnelGroupBox"
        Me.OptionnelGroupBox.Size = New System.Drawing.Size(178, 89)
        Me.OptionnelGroupBox.TabIndex = 11
        Me.OptionnelGroupBox.TabStop = False
        Me.OptionnelGroupBox.Text = "Période"
        '
        'TxtToDate
        '
        Me.TxtToDate.AutoSize = True
        Me.TxtToDate.Location = New System.Drawing.Point(15, 57)
        Me.TxtToDate.Name = "TxtToDate"
        Me.TxtToDate.Size = New System.Drawing.Size(13, 13)
        Me.TxtToDate.TabIndex = 9
        Me.TxtToDate.Text = "à"
        '
        'TxtFromDate
        '
        Me.TxtFromDate.AutoSize = True
        Me.TxtFromDate.Location = New System.Drawing.Point(9, 22)
        Me.TxtFromDate.Name = "TxtFromDate"
        Me.TxtFromDate.Size = New System.Drawing.Size(21, 13)
        Me.TxtFromDate.TabIndex = 8
        Me.TxtFromDate.Text = "De"
        '
        'ToDate
        '
        Me.ToDate.CustomFormat = ""
        Me.ToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.ToDate.Location = New System.Drawing.Point(84, 57)
        Me.ToDate.Name = "ToDate"
        Me.ToDate.Size = New System.Drawing.Size(82, 20)
        Me.ToDate.TabIndex = 7
        Me.ToDate.Value = New Date(2020, 1, 1, 0, 0, 0, 0)
        '
        'FromDate
        '
        Me.FromDate.CustomFormat = ""
        Me.FromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FromDate.Location = New System.Drawing.Point(84, 16)
        Me.FromDate.Name = "FromDate"
        Me.FromDate.Size = New System.Drawing.Size(80, 20)
        Me.FromDate.TabIndex = 6
        Me.FromDate.Value = New Date(2020, 1, 1, 0, 0, 0, 0)
        '
        'FiltreDate
        '
        Me.FiltreDate.AutoSize = True
        Me.FiltreDate.Location = New System.Drawing.Point(6, 47)
        Me.FiltreDate.Name = "FiltreDate"
        Me.FiltreDate.Size = New System.Drawing.Size(107, 17)
        Me.FiltreDate.TabIndex = 13
        Me.FiltreDate.Text = "Filtrer par période"
        Me.FiltreDate.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Ou)
        Me.GroupBox3.Controls.Add(Me.ArtLib)
        Me.GroupBox3.Controls.Add(Me.LibArticle)
        Me.GroupBox3.Controls.Add(Me.ArtRef)
        Me.GroupBox3.Controls.Add(Me.RefArticle)
        Me.GroupBox3.Location = New System.Drawing.Point(6, 25)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(248, 70)
        Me.GroupBox3.TabIndex = 12
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Obligatoire"
        '
        'Ou
        '
        Me.Ou.AutoSize = True
        Me.Ou.Location = New System.Drawing.Point(112, 47)
        Me.Ou.Name = "Ou"
        Me.Ou.Size = New System.Drawing.Size(21, 13)
        Me.Ou.TabIndex = 10
        Me.Ou.Text = "Ou"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(12, 159)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView1.Size = New System.Drawing.Size(1085, 483)
        Me.DataGridView1.TabIndex = 6
        '
        'GetExcel
        '
        Me.GetExcel.Location = New System.Drawing.Point(66, 48)
        Me.GetExcel.Name = "GetExcel"
        Me.GetExcel.Size = New System.Drawing.Size(124, 25)
        Me.GetExcel.TabIndex = 7
        Me.GetExcel.Text = "Générer un Excel"
        Me.GetExcel.UseVisualStyleBackColor = True
        '
        'ExcelGroupBox
        '
        Me.ExcelGroupBox.Controls.Add(Me.GetExcel)
        Me.ExcelGroupBox.Location = New System.Drawing.Point(817, 11)
        Me.ExcelGroupBox.Name = "ExcelGroupBox"
        Me.ExcelGroupBox.Size = New System.Drawing.Size(215, 123)
        Me.ExcelGroupBox.TabIndex = 8
        Me.ExcelGroupBox.TabStop = False
        Me.ExcelGroupBox.Text = "GroupBox4"
        '
        'ArtError
        '
        Me.ArtError.AutoSize = True
        Me.ArtError.Location = New System.Drawing.Point(12, 137)
        Me.ArtError.Name = "ArtError"
        Me.ArtError.Size = New System.Drawing.Size(52, 13)
        Me.ArtError.TabIndex = 9
        Me.ArtError.Text = "               "
        '
        'ExcelError
        '
        Me.ExcelError.AutoSize = True
        Me.ExcelError.Location = New System.Drawing.Point(814, 137)
        Me.ExcelError.Name = "ExcelError"
        Me.ExcelError.Size = New System.Drawing.Size(52, 13)
        Me.ExcelError.TabIndex = 10
        Me.ExcelError.Text = "               "
        '
        'CheckExcel
        '
        Me.CheckExcel.AutoSize = True
        Me.CheckExcel.Location = New System.Drawing.Point(684, 67)
        Me.CheckExcel.Name = "CheckExcel"
        Me.CheckExcel.Size = New System.Drawing.Size(127, 17)
        Me.CheckExcel.TabIndex = 11
        Me.CheckExcel.Text = "Générer Fichier Excel"
        Me.CheckExcel.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1109, 654)
        Me.Controls.Add(Me.CheckExcel)
        Me.Controls.Add(Me.ExcelError)
        Me.Controls.Add(Me.ArtError)
        Me.Controls.Add(Me.ExcelGroupBox)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.SearchGroupBox)
        Me.Name = "Form1"
        Me.Text = "Get Customers By Article"
        Me.SearchGroupBox.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.OptionnelGroupBox.ResumeLayout(False)
        Me.OptionnelGroupBox.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ExcelGroupBox.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents RefArticle As System.Windows.Forms.Label
    Friend WithEvents ArtRef As System.Windows.Forms.TextBox
    Friend WithEvents LibArticle As System.Windows.Forms.Label
    Friend WithEvents ArtLib As System.Windows.Forms.TextBox
    Friend WithEvents GetData As System.Windows.Forms.Button
    Friend WithEvents SearchGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents ToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents FromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents TxtToDate As System.Windows.Forms.Label
    Friend WithEvents TxtFromDate As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Ou As System.Windows.Forms.Label
    Friend WithEvents OptionnelGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents GetExcel As System.Windows.Forms.Button
    Friend WithEvents FiltreDate As System.Windows.Forms.CheckBox
    Friend WithEvents ExcelGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents ArtError As System.Windows.Forms.Label
    Friend WithEvents ExcelError As System.Windows.Forms.Label
    Friend WithEvents CheckExcel As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Reload As System.Windows.Forms.Button

End Class
