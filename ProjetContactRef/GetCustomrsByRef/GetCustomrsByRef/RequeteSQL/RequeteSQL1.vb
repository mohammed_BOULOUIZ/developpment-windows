﻿Module RequetSQLi

    Public requetSQLRef As String = "Select [F_ARTICLE].AR_ref as Référence, [AR_Design] as Designation, [F_DOCLIGNE].DO_Date as Date , CT_Intitule as Nom, CT_Adresse as Adresse, CT_Complement as Complement, CT_CodePostal as CodePostal, CT_Ville as Ville," + _
           " CT_Pays as Pays,  CT_Telephone as Tel, ISNULL(CT_EMail, 'NC') as Email from [F_COMPTET] " + _
           " left join [F_DOCLIGNE] on [F_COMPTET].CT_Num = [F_DOCLIGNE].CT_Num" + _
           " left join [F_ARTICLE] on [F_DOCLIGNE].AR_ref = [F_ARTICLE].AR_ref" + _
           " Where [F_ARTICLE].AR_ref = '{0}' and DO_Domaine = 0 and  DO_Type = 7" + _
           " order by date DESC"

    Public requetSQLLib As String = "Select [F_ARTICLE].AR_ref as Référence,[AR_Design] as Designation, [F_DOCLIGNE].DO_Date as Date , CT_Intitule as Nom, CT_Adresse as Adresse, CT_Complement as Complement, CT_CodePostal as CodePostal, CT_Ville as Ville," + _
           " CT_Pays as Pays,  CT_Telephone as Tel, ISNULL(CT_EMail, 'NC') as Email from [F_COMPTET] " + _
           " left join [F_DOCLIGNE] on [F_COMPTET].CT_Num = [F_DOCLIGNE].CT_Num" + _
           " left join [F_ARTICLE] on [F_DOCLIGNE].AR_ref = [F_ARTICLE].AR_ref" + _
           " Where [F_ARTICLE].AR_Design = '{0}' and DO_Domaine = 0 and  DO_Type = 7" + _
           " order by date DESC"

    Public requetSQLRefDates As String = "Select [F_ARTICLE].AR_ref as Référence,[AR_Design] as Designation, [F_DOCLIGNE].DO_Date as Date , CT_Intitule as Nom, CT_Adresse as Adresse, CT_Complement as Complement, CT_CodePostal as CodePostal, CT_Ville as Ville," + _
           " CT_Pays as Pays,  CT_Telephone as Tel, ISNULL(CT_EMail, 'NC') as Email from [F_COMPTET] " + _
           " left join [F_DOCLIGNE] on [F_COMPTET].CT_Num = [F_DOCLIGNE].CT_Num" + _
           " left join [F_ARTICLE] on [F_DOCLIGNE].AR_ref = [F_ARTICLE].AR_ref" + _
           " Where [F_ARTICLE].AR_ref = '{0}' and DO_Domaine = 0 and  DO_Type = 7 and [F_DOCLIGNE].DO_Date BETWEEN '{1}' AND '{2}'" + _
           " order by date DESC"

    Public requetSQLLibDates As String = "Select [F_ARTICLE].AR_ref as Référence,[AR_Design] as Designation, [F_DOCLIGNE].DO_Date as Date , CT_Intitule as Nom, CT_Adresse as Adresse, CT_Complement as Complement, CT_CodePostal as CodePostal, CT_Ville as Ville," + _
           " CT_Pays as Pays,  CT_Telephone as Tel, ISNULL(CT_EMail, 'NC') as Email from [F_COMPTET] " + _
           " left join [F_DOCLIGNE] on [F_COMPTET].CT_Num = [F_DOCLIGNE].CT_Num" + _
           " left join [F_ARTICLE] on [F_DOCLIGNE].AR_ref = [F_ARTICLE].AR_ref" + _
           " Where [F_ARTICLE].AR_Design = '{0}' and DO_Domaine = 0 and  DO_Type = 7 and [F_DOCLIGNE].DO_Date BETWEEN '{1}' AND '{2}'" + _
           " order by date DESC"

    Public requetSQLGetAllByRef As String = "select count(AR_Ref) from F_ARTICLE where AR_ref = '{0}'"
    Public requetSQLGetAllByLib As String = "select count(AR_Design) from F_ARTICLE where AR_Design = '{0}'"

End Module
