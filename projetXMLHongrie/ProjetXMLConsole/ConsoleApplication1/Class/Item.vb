﻿Imports ConsoleApplicationHongrie.Pricing
Imports ConsoleApplicationHongrie.Storage
Imports ConsoleApplicationHongrie.Product

Public Class Item

    '**********Pricing**********
    Private _pricing As Pricing
    Public Property Pricing() As Pricing
        Get
            Return _pricing
        End Get
        Set(ByVal value As Pricing)
            _pricing = value
        End Set
    End Property

    '**********Storage**********
    Private _storage As Storage
    Public Property Storage() As Storage
        Get
            Return _storage
        End Get
        Set(ByVal value As Storage)
            _storage = value
        End Set
    End Property

    '**********Product**********
    Private _product As Product
    Public Property Product() As Product
        Get
            Return _product
        End Get
        Set(ByVal value As Product)
            _product = value
        End Set
    End Property


    '**********Constructeur 1**********
    Sub New()
        _pricing = New Pricing
        _storage = New Storage
        _product = New Product

    End Sub

End Class

