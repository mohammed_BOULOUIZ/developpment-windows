﻿Public Class Pricing
    '**********Price€ Produit**********
    '' 1er prix
    Private _priceWithFee As Double
    Public Property PriceWithFee() As Double
        Get
            Return _priceWithFee
        End Get
        Set(ByVal value As Double)
            _priceWithFee = value
        End Set
    End Property

    ''2emeprix
    Private _priceWithoutFee As Double
    Public Property PriceWithoutFee() As Double
        Get
            Return _priceWithoutFee
        End Get
        Set(ByVal value As Double)
            _priceWithoutFee = value
        End Set
    End Property

    ''3emeprix
    Private _recycleFee As Double
    Public Property RecycleFee() As Double
        Get
            Return _priceWithoutFee
        End Get
        Set(ByVal value As Double)
            _priceWithoutFee = value
        End Set
    End Property

    ''4emeprix
    Private _copyrightFee As Double
    Public Property CopyrightFee() As Double
        Get
            Return _priceWithoutFee
        End Get
        Set(ByVal value As Double)
            _priceWithoutFee = value
        End Set
    End Property

    '**********Devis€ Produit**********
    Private _currency As String
    Public Property Currency() As String
        Get
            Return _currency
        End Get
        Set(ByVal value As String)
            _currency = value
        End Set
    End Property


    '**********Constructeur 1**********
    Sub New()
        _priceWithFee = 0.0
        _priceWithoutFee = 0.0
        _recycleFee = 0.0
        _copyrightFee = 0.0
        _currency = String.Empty
    End Sub
End Class
