﻿Module RequetSQL
    Public requetSQL As String = "select [F_ARTICLE].AR_Ref, [F_ARTICLE].AR_Design,ISNULL([F_ARTGAMME].EG_Enumere, '') as EG_Enumere" + _
  " ,Case" + _
     " When F_ARTICLE.AR_Gamme1 = 0 then [F_ARTICLE].AR_CodeBarre when F_ARTICLE.AR_Gamme1 <> 0 then [F_ARTENUMREF].AE_CodeBarre" + _
  " end as CodeBarre" + _
  " ,Case When F_ARTICLE.AR_Gamme1 = 0 then" + _
            " ISNULL([F_ARTSTOCK].AS_QteSto, 0)" + _
        " else" + _
            " ISNULL([F_GAMSTOCK].GS_QteSto, 0)" + _
  " end as Stock" + _
  " ,Case When [F_ARTCLIENT].[AC_PrixVen] IS NULL then" + _
            " [F_ARTICLE].AR_PrixVen" + _
        " else" + _
            " [F_ARTCLIENT].[AC_PrixVen]" + _
  " end as PrixVenteExpo" + _
 " from [F_ARTICLE]" + _
 " left join [F_ARTGAMME] on [F_ARTICLE].AR_Ref = [F_ARTGAMME].AR_Ref" + _
 " left join [F_ARTENUMREF] on [F_ARTENUMREF].AR_Ref =  [F_ARTGAMME].AR_Ref and [F_ARTENUMREF].AG_No1 =  [F_ARTGAMME].AG_No" + _
 " inner join [F_ARTSTOCK] on [F_ARTICLE].AR_Ref = [F_ARTSTOCK].AR_Ref and DE_No = 1 " + _
 " left join [F_GAMSTOCK] on [F_ARTICLE].AR_Ref = [F_GAMSTOCK].AR_Ref and [F_GAMSTOCK].AG_No1 = [F_ARTENUMREF].AG_No1 and [F_GAMSTOCK].DE_No = 1 " + _
 " left join [F_ARTCLIENT] on [F_ARTICLE].AR_Ref = [F_ARTCLIENT].AR_Ref and [F_ARTCLIENT].AC_Categorie = 6 " + _
 " where [F_ARTICLE].AR_Sommeil = 0 and left ([F_ARTICLE].AR_Ref, 3) = 'ABY' and [F_ARTICLE].FA_CodeFamille not in ('ABYDIV', 'DIVPLV') and" + _
    " Case When F_ARTICLE.AR_Gamme1 = 0 and [F_ARTENUMREF].AE_CodeBarre is null then [F_ARTICLE].AR_CodeBarre" + _
    " when F_ARTICLE.AR_Gamme1 <> 0 and [F_ARTICLE].AR_CodeBarre is null then [F_ARTENUMREF].AE_CodeBarre" + _
 " end is not null" + _
" ORDER BY AR_Ref"

End Module
