select  [F_ARTICLE].AR_Ref, [F_ARTICLE].AR_Design
		
		,ISNULL([F_ARTGAMME].EG_Enumere, '') as EG_Enumere

		,Case 
			  When F_ARTICLE.AR_Gamme1 = 0 then
					[F_ARTICLE].AR_CodeBarre
			  when F_ARTICLE.AR_Gamme1 <> 0 then
					[F_ARTENUMREF].AE_CodeBarre
		end as CodeBarre

		,Case When F_ARTICLE.AR_Gamme1 = 0 then
			ISNULL([F_ARTSTOCK].AS_QteSto, 0)
			else
			ISNULL([F_GAMSTOCK].GS_QteSto, 0)
			end as Stock

		,Case When [F_ARTCLIENT].[AC_PrixVen] IS NULL then 
			[F_ARTICLE].AR_PrixVen
			else
			[F_ARTCLIENT].[AC_PrixVen]
			end as PrixVenteExpo
		 
				 

from [F_ARTICLE] 

left join [F_ARTGAMME] on [F_ARTICLE].AR_Ref = [F_ARTGAMME].AR_Ref

left join [F_ARTENUMREF] on [F_ARTENUMREF].AR_Ref =  [F_ARTGAMME].AR_Ref and [F_ARTENUMREF].AG_No1 =  [F_ARTGAMME].AG_No

inner join [F_ARTSTOCK] on [F_ARTICLE].AR_Ref = [F_ARTSTOCK].AR_Ref and DE_No = 1 -- DE_No = 1 signifie qu'on utilise le depot 1

left join [F_GAMSTOCK] on [F_ARTICLE].AR_Ref = [F_GAMSTOCK].AR_Ref and [F_GAMSTOCK].AG_No1 = [F_ARTENUMREF].AG_No1 and [F_GAMSTOCK].DE_No = 1  -- DE_No = 1 signifie qu'on utilise le depot 1

left join [F_ARTCLIENT] on [F_ARTICLE].AR_Ref = [F_ARTCLIENT].AR_Ref and [F_ARTCLIENT].AC_Categorie = 6 -- AC_Categorie = 6 signifie que ces produit sont destin� � l'export

where [F_ARTICLE].AR_Sommeil = 0 and left ([F_ARTICLE].AR_Ref, 3) = 'ABY' and [F_ARTICLE].FA_CodeFamille not in ('ABYDIV', 'DIVPLV') 

and 

Case When F_ARTICLE.AR_Gamme1 = 0 and [F_ARTENUMREF].AE_CodeBarre is null then
					[F_ARTICLE].AR_CodeBarre
	 when F_ARTICLE.AR_Gamme1 <> 0 and [F_ARTICLE].AR_CodeBarre is null then
					[F_ARTENUMREF].AE_CodeBarre	  
end is not null




ORDER BY AR_Ref

