﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Xml.Serialization

Imports System.Timers
Imports JV_Fonctions



Public Class ServiceGetArticle
    Dim parametre As New Parametre
    Dim base As String
    Dim instance As String
    Dim ftp As String

    'chemin du repertoire ou se trouve le fichier sources
    Dim path As String = My.Application.Info.DirectoryPath
    Dim directoryData As String = path + "\Data\"
    Dim xmlParam As String = directoryData + "xmlParam.xml"
    Dim xmlArticle As String = directoryData + "xmlArticle.xml"

    Dim cnx As SqlConnection
    Dim cmd As SqlCommand
    Dim dread As SqlDataReader

    Dim timer As New Timer
    Dim nowHour As Integer
    Dim errMessage As String

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Ajoutez ici le code pour démarrer votre service. Cette méthode doit
        ' démarrer votre service.
        EventLog.WriteEntry("Le service Getarticle a démarré")


        Try
            GetArticleExpo()
            timer.Interval = 3600000 '1h
            AddHandler timer.Elapsed, AddressOf RunService
            timer.Enabled = True

        Catch ex As Exception
            errMessage = ex.Message
            JV_Fonctions.FJV.FichierErreur(errMessage, , )
        End Try

    End Sub

    Private Sub RunService()
        nowHour = DateTime.Now.TimeOfDay.Hours
        If nowHour = 17 Or nowHour = 1 Then
            GetArticleExpo()
        End If
    End Sub

    Protected Overrides Sub OnStop()
        ' Ajoutez ici le code pour effectuer les destructions nécessaires à l'arrêt de votre service.
        Try
            EventLog.WriteEntry("Le service Getarticle s'est arrêté ")
            timer.Enabled = False
        Catch ex As Exception
            errMessage = ex.Message
            JV_Fonctions.FJV.FichierErreur(errMessage, , )
        End Try

    End Sub

    Protected Sub GetArticleExpo()

        
        If (Not Directory.Exists(directoryData)) Then
            System.IO.Directory.CreateDirectory(directoryData)
        End If
        If (Directory.Exists(directoryData) And File.Exists(xmlParam)) Then

            base = GetBase(xmlParam, parametre)
            instance = GetInstance(xmlParam, parametre)
            ftp = GetFtp(xmlParam)

            Try
                cnx = New SqlConnection("Data Source=" + instance + "; initial catalog=" + base + "; Integrated Security=True")
                cmd = cnx.CreateCommand
                cmd.CommandText = RequetSQL.requetSQL
                cnx.Open()
                dread = cmd.ExecuteReader

                XMLWriter(dread)

                dread.Close()
                cnx.Close()
                'fonction qui permet d'envoyer le fichier 'xmlArticle.xml' vers le Ftp
                SendToFtpDir(xmlArticle, ftp)
            Catch ex As Exception
                'Catch le message d'erreur et envoi vers un fichier log via la fonction FichierErreur()
                errMessage = "voici le message: " & ex.Message
                JV_Fonctions.FJV.FichierErreur(errMessage, , )
                JV_Fonctions.FJV.FichierErreur(ex.StackTrace, , )
                JV_Fonctions.FJV.FichierErreur(base, , )
                JV_Fonctions.FJV.FichierErreur(instance, , )
            End Try

        Else
            JV_Fonctions.FJV.FichierErreur("xmlParam ou son dossier 'Data' sont inéxistants", , )
        End If


    End Sub

    'la fonction XMLWriter reçoit des des variables depuis la boucle While 
    Private Function XMLWriter(ByVal dread As Object)

        'instanciation d'une liste qui permet de regrouper tous les items[ pour remplir la liste]
        Dim itemList As New List(Of Item)
        'instanciation d'une variable qui recupére le contenu du message
        Dim errMessage As String
        'instanciation de la variable LimiteStock
        Dim limiteStock As Integer
        limiteStock = GetLimiteStock(xmlParam, parametre)



        'Normalisation de la class Item en XML dans la variable "writer"
        Dim ArrayOfItem As New XmlRootAttribute("items")
        Dim writer As New System.Xml.Serialization.XmlSerializer(GetType(List(Of Item)), ArrayOfItem)


        Try
            'definition de lURL et le fichier qui stockera les données

            Dim xmlFile As New System.IO.StreamWriter(directoryData + "xmlArticle" + ".xml")

            'Tant qu'il y a des données, les ecrire dans le fichier spécifié "file"

            While (dread.Read())
                'instanxciation de la class item=>article pour ajouter à chaque fois une nouvel objet
                Dim item As New Item
                Dim checkStockNull As New Boolean

                '**********Pricing**********
                item.Pricing.PriceWithoutFee = dread.Item(5)
                item.Pricing.Currency = "EUR"

                '**********Storage**********
                item.Storage.StoredQuantity = dread.Item(4)

                checkStockNull = IsDBNull(item.Storage.StoredQuantity)
                If item.Storage.StoredQuantity < limiteStock Or checkStockNull = True Then
                    item.Storage.StoredQuantity = 0
                Else
                    item.Storage.StoredQuantity = limiteStock
                End If


                '**********Product**********
                item.Product.Name = dread.Item(1)
                item.Product.DealerCode = dread.Item(0)
                item.Product.Range = dread.Item(2)
                item.Product.Ean = dread.Item(3)

                If item.Product.Range = Nothing Then
                    item.Product.Range = ""
                End If

                'jouter à chaque fois un nouvel objet
                itemList.Add(item)
            End While


            'enregistrement dans le fichier vers le repértoire associés
            writer.Serialize(xmlFile, itemList)
            xmlFile.Close()


        Catch ex As Exception
            'Catch le message d'erreur et envoi vers un fichier log via la fonction FichierErreur()
            errMessage = "Probleme d'exécution de la fonction privée XMLWriter, voici le message: " & ex.Message
            JV_Fonctions.FJV.FichierErreur(errMessage, , )
        End Try
        Return Nothing

    End Function

    'cette fonction utilise la fonction ReadXML pour lire le xml et en extraire la variable Base (base de donnée)
    Private Function GetBase(ByVal paramXml As String, ByVal Objet As Object)
        Dim fichierGestComm As String
        Dim obj As New Object
        Dim base As String

        obj = ReadXML(paramXml)
        fichierGestComm = obj.fichierGestComm
        base = FJV.BaseSqlFic(fichierGestComm)

        Return base
    End Function

    'cette fonction utilise la fonction ReadXML pour lire le xml et en extraire la variable Instance (serveur sql)
    Private Function GetInstance(ByVal paramXml As String, ByVal Objet As Object)
        Dim fichierGestComm As String
        Dim obj As New Object
        Dim instance As String

        obj = ReadXML(paramXml)
        fichierGestComm = obj.fichierGestComm
        instance = FJV.InstanceSqlFic(fichierGestComm)

        Return instance
    End Function

    'cette fonction utilise la fonction ReadXML pour lire le xml et en extraire la variable LimiteStock (limite de stock qui sera afficher dans le XML Articles)
    Private Function GetLimiteStock(ByVal paramXml As String, ByVal Objet As Object)
        Dim limiteStock As Integer
        Dim obj As New Object

        obj = ReadXML(paramXml)
        limiteStock = obj.limiteStock

        Return limiteStock
    End Function

    'cette fonciton permet d'extraire le parametre ftp du fichier xmlparam
    Private Function GetFtp(ByVal file As String)
        Dim reader As New XmlSerializer(GetType(Parametre))
        Dim xmlFile As New StreamReader(file)
        Dim parametre As Parametre
        Dim ftp As String

        parametre = reader.Deserialize(xmlFile)
        ftp = parametre.Ftp
        Return ftp
    End Function

    'cette fonciton reçoit le fichier xml et en extrait son contenu dans objet via la fonction 'DESERIALIZE'
    Private Function ReadXML(ByVal file As String)
        Dim reader As New XmlSerializer(GetType(Parametre))
        Dim xmlFile As New StreamReader(file)
        Dim parametre As Parametre
        parametre = reader.Deserialize(xmlFile)
        Return parametre
    End Function

    'Cette fonction permet d'envoyer le fichier xmlArticle.xml vers le Ftp 
    Private Sub SendToFtpDir(ByVal xmlArticle As String, ByVal ftpDir As String)
        Dim ftpFile As String = ftpDir + "\Prices.xml"
        If (File.Exists(ftpFile)) Then
            File.Delete(ftpFile)
        End If
        Try
            My.Computer.FileSystem.CopyFile(xmlArticle, ftpFile, FileIO.UIOption.OnlyErrorDialogs, FileIO.UICancelOption.DoNothing)
        Catch ex As Exception
            errMessage = "Probleme de transfert du fichier xmlArticle vers serveur Ftp, voici le message: " & ex.Message
            JV_Fonctions.FJV.FichierErreur(errMessage, , )

        End Try
    End Sub

End Class


