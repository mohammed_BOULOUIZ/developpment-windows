﻿Public Class Parametre


    Private _fichierGestComm As String
    Public Property FichierGestComm() As String
        Get
            Return _fichierGestComm
        End Get
        Set(ByVal value As String)
            _fichierGestComm = value
        End Set
    End Property

    Private _limiteStock As String
    Public Property LimiteStock() As String
        Get
            Return _limiteStock
        End Get
        Set(ByVal value As String)
            _limiteStock = value
        End Set
    End Property

    Private _ftp As String
    Public Property Ftp() As String
        Get
            Return _ftp
        End Get
        Set(ByVal value As String)
            _ftp = value
        End Set
    End Property

    Sub New()
        _fichierGestComm = String.Empty
        _limiteStock = String.Empty
        _ftp = String.Empty
    End Sub
End Class
