﻿Public Class Storage

    '**********Stock Produit**********
    Private _storedQuantity As Integer
    Public Property StoredQuantity() As Integer
        Get
            Return _storedQuantity
        End Get
        Set(ByVal value As Integer)
            _storedQuantity = value
        End Set
    End Property

    '**********Constructeur 1**********
    Sub New()
        _storedQuantity = 0
    End Sub
End Class
