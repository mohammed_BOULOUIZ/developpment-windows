﻿Public Class Product

    '**********Designation Produit**********
    Private _name As String
    Public Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property

    '**********référence Produit**********
    Private _dealerCode As String
    Public Property DealerCode() As String
        Get
            Return _dealerCode
        End Get
        Set(ByVal value As String)
            _dealerCode = value
        End Set
    End Property

    '**********NumeroPart? Produit**********
    Private _partNumber As Integer
    Public Property PartNumber() As Integer
        Get
            Return _partNumber
        End Get
        Set(ByVal value As Integer)
            _partNumber = value
        End Set
    End Property

    '**********gammr Produit**********
    Private _range As String
    Public Property Range() As String
        Get
            Return _range
        End Get
        Set(ByVal value As String)
            _range = value
        End Set
    End Property


    '**********Code barre Produit**********
    Private _ean As String
    Public Property Ean() As String
        Get
            Return _ean
        End Get
        Set(ByVal value As String)
            _ean = value
        End Set
    End Property


    Sub New()
        _name = String.Empty
        _dealerCode = String.Empty
        _partNumber = 0
        _range = String.Empty
        _ean = String.Empty


    End Sub


End Class
