﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports JV_Fonctions
Imports System.Xml.Serialization

Public Class Form1

    Dim fichierGestComm As String
    Dim errMessage As String
    Dim fileName As String

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ErrSucc.Hide()
        LinkLabel1.Hide()
        Dim path As String = Application.StartupPath
        Dim parametre As Parametre
        Dim directoryData As String = path + "\Data\"
        Dim xmlParamFile As String = directoryData + "xmlParam" + ".xml"

        If Directory.Exists(directoryData) And File.Exists(xmlParamFile) Then
            parametre = ReadXML(xmlParamFile)
            GesComm.Text = parametre.FichierGestComm
            LimiteStock.Text = parametre.LimiteStock
            Ftp.Text = parametre.Ftp
            xmlParamFile = ""
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim choixDialog As New OpenFileDialog
        ErrSucc.Hide()
        LinkLabel1.Hide()
        choixDialog.FileName = Nothing

        choixDialog.Filter = "fichier de gestion commerciale|*.gcm"
        GesComm.ReadOnly = True
        If choixDialog.ShowDialog() = 1 Then
            ErrSucc.Hide()
            fileName = choixDialog.FileName
            GesComm.Text = fileName
            Try
                fichierGestComm = GesComm.Text
                'base = JV_Fonctions.FJV.BaseSqlFic(GesComm.Text) 'bdd
                'instance = JV_Fonctions.FJV.InstanceSqlFic(GesComm.Text) 'data source
            Catch ex As Exception
                JV_Fonctions.FJV.FichierErreur(ex.Message)
            End Try
        End If

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        Dim routeParam As New XmlRootAttribute("Parametre")
        Dim writer As New System.Xml.Serialization.XmlSerializer(GetType(Parametre), routeParam)
        Dim param As New Parametre

        Dim path As String = Application.StartupPath
        Dim directoryData As String = Path + "\Data\"


        'convertir en int la valeur du textbox si il y a une valeur, car le parse de Nothing peut créer une excéption
        Dim parsInt As Integer

        'parser le contenu du champ stock en integer
        If LimiteStock.Text <> Nothing Then
            parsInt = Integer.Parse(LimiteStock.Text)
        End If

        Try
            If GesComm.Text = "" Or GesComm.Text = Nothing Or GesComm.Text <> fileName Then
                fichierGestComm = ""
            End If
            If (Not Directory.Exists(directoryData)) Then
                System.IO.Directory.CreateDirectory(directoryData)
            End If

            If fichierGestComm <> "" Or fichierGestComm <> Nothing Then

                If LimiteStock.Text <> Nothing And parsInt > 0 Then

                    If Ftp.Text <> "" Or Ftp.Text <> Nothing Then

                        Dim xmlParamFile As New System.IO.StreamWriter(directoryData + "xmlParam" + ".xml")
                        param.FichierGestComm = fichierGestComm
                        param.LimiteStock = LimiteStock.Text
                        param.Ftp = Ftp.Text
                        writer.Serialize(xmlParamFile, param)
                        xmlParamFile.Close()

                        'post creation du fichier param_xml avec succés
                        ErrSucc.Text = " le fichier xmlParam.xml a été crée avec SUCCES"
                        ErrSucc.ForeColor = Color.Green
                        ErrSucc.Show()
                        GesComm.Clear()
                        LimiteStock.Clear()
                        Ftp.Clear()
                        LinkLabel1.Show()
                    Else
                        'post creation du fichier param_xml avec erreur de données fichier de gestion commerciale
                        ErrSucc.Text = "le fichier xmlParam.xml n'a pas pu etre crée, car le lien FTP est manquant"
                        ErrSucc.ForeColor = Color.Red
                        ErrSucc.Show()

                    End If
                Else
                    'post creation du fichier param_xml avec erreur de données fichier de gestion commerciale
                    ErrSucc.Text = "le fichier xmlParam.xml n'a pas pu etre crée," + vbCrLf + "car la limite de stock n'est pas définie"
                    ErrSucc.ForeColor = Color.Red
                    ErrSucc.Show()
                End If
            Else
                'post creation du fichier param_xml avec erreur de données fichier de gestion commerciale
                ErrSucc.Text = "le fichier xmlParam.xml n'a pas pu etre crée," + vbCrLf + "car le fichier de gesiton commercial est indisponible ou sans données"
                ErrSucc.ForeColor = Color.Red
                ErrSucc.Show()
            End If

        Catch ex As Exception
            errMessage = ex.Message
            JV_Fonctions.FJV.FichierErreur(ex.Message)
        End Try
        If errMessage <> "" Or errMessage <> Nothing Then
            'post creation du fichier param_xml avec erreur catchée
            ErrSucc.Text = "le fichier xmlParam.xml n'a pas pu etre crée," + vbCrLf + errMessage
            ErrSucc.ForeColor = Color.Red
            ErrSucc.Show()
        End If

    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Dim path As String = Application.StartupPath
        Dim directoryData As String = path + "\Data\"
        LinkLabel1.LinkVisited = True
        System.Diagnostics.Process.Start(directoryData + "xmlParam" + ".xml")
    End Sub

    Private Sub LimiteStock_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles LimiteStock.Validating
        If IsNumeric(LimiteStock.Text) = False Then
            LimiteStock.Select(0, LimiteStock.Text.Length)
            e.Cancel = True
        End If

    End Sub

    Private Function ReadXML(ByVal file As String)
        Dim reader As New XmlSerializer(GetType(Parametre))
        Dim xmlFile As New StreamReader(file)
        Dim parametre As Parametre

        parametre = reader.Deserialize(xmlFile)
        xmlFile.Close()
        Return parametre
    End Function
End Class
