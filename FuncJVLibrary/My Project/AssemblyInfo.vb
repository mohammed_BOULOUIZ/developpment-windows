﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' Les informations générales relatives à un assembly dépendent de 
' l'ensemble d'attributs suivant. Changez les valeurs de ces attributs pour modifier les informations
' associées à un assembly.

' Vérifiez les valeurs des attributs de l'assembly

<Assembly: AssemblyTitle("FuncJVLibrary")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Jean VOLPE")> 
<Assembly: AssemblyProduct("FuncJVLibrary")> 
<Assembly: AssemblyCopyright("Copyright ©  2013")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>
<Assembly: CLSCompliant(True)> 
'Le GUID suivant est pour l'ID de la typelib si ce projet est exposé à COM
<Assembly: Guid("cd661a66-57f1-41c2-86a9-7258be92acb2")> 

' Les informations de version pour un assembly se composent des quatre valeurs suivantes :
'
'      Version principale
'      Version secondaire 
'      Numéro de build
'      Révision
'
' Vous pouvez spécifier toutes les valeurs ou indiquer les numéros de build et de révision par défaut 
' en utilisant '*', comme indiqué ci-dessous :
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("2.0.0.0")> 
<Assembly: AssemblyFileVersion("2.0.0.0")> 
