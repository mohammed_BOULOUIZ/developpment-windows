﻿Imports System
Imports System.IO
Imports System.Net.Mail
Imports System.Environment
Imports System.Xml

Public Class FJV
    Public Class ParamsMail
        Public Shared Sender As String
        Public Shared SmtpServer As String
        Public Shared SenderUser As String
        Public Shared SenderPwd As String
        Public Shared SMTPPort As String
        Public Shared Dest As String
        Public Shared DestCopy As String
        Public Shared SSL As String
        Public Shared AUTH As String
    End Class
    Public Shared MyParMail As New ParamsMail

    Public Shared Function DateAccess(ByVal DaFr As String) As String
        DateAccess = ""
        If DaFr <> "" Then DateAccess = Mid(DaFr, 4, 2) & "/" & Mid(DaFr, 1, 2) & "/" & Mid(DaFr, 7, 4)
    End Function
    Public Shared Function CodePwd(ByVal Password As String) As String
        Dim Pwd As String = String.Empty
        Dim LongPwd As Integer = 0
        Dim Pwdtemp As String = String.Empty
        Dim I As Integer = 0
        CodePwd = ""

        Try
            If Password IsNot String.Empty Then
                LongPwd = Len(Password)
                Pwdtemp = Password
                For I = 1 To 50 - LongPwd
                    Pwdtemp = Pwdtemp + Chr(40 + I)
                Next

                For I = 1 To 25
                    Pwd = Pwd + Hex(Asc(Mid(Pwdtemp, I, 1)))
                    Pwd = Pwd + Hex(Asc(Mid(Pwdtemp, 51 - I, 1)))
                Next
                Pwd = Pwd + Hex(LongPwd)
            Else
                Pwd = String.Empty
            End If

            CodePwd = Pwd
        Catch ex As Exception
            Call FichierErreur(ex.Message)
        End Try
    End Function

    Public Shared Function DecodePwd(ByVal Password As String) As String
        Dim Pwd As String = String.Empty
        Dim PwdTemp As String = String.Empty
        Dim LongPwd As Integer = 0
        Dim I As Integer = 0
        DecodePwd = ""
        Try
            If Password IsNot String.Empty Then
                LongPwd = "&H" & Mid(Password, 101, 2)
                For I = 0 To 96 Step 4
                    PwdTemp = PwdTemp + Chr("&H" & (Mid(Password, I + 1, 2)))
                Next
                For I = 98 To 2 Step -4
                    PwdTemp = PwdTemp + Chr("&H" & (Mid(Password, I + 1, 2)))
                Next
                Pwd = Mid(PwdTemp, 1, LongPwd)
            Else
                Pwd = String.Empty
            End If

            DecodePwd = Pwd
        Catch ex As Exception
            Call FichierErreur(ex.Message)
        End Try

    End Function
    Public Shared Function DirOf(ByVal Fichier As String) As String
        Dim PathOf As String = String.Empty
        DirOf = ""

        Try
            Dim I As Integer

            If Fichier <> "" Then
                For I = 1 To Len(Fichier)
                    If Mid(Fichier, I, 1) = "\" Then PathOf = Mid(Fichier, 1, I - 1)
                Next
            Else
                PathOf = ""
            End If

            DirOf = PathOf
        Catch ex As Exception
            Call FichierErreur(ex.Message)
        End Try
    End Function
    Public Shared Function NameFicOf(ByVal Fichier As String) As String
        Dim NameFic As String = String.Empty
        Dim I As Integer
        NameFicOf = ""
        Try
            If Fichier <> "" Then
                For I = 1 To Len(Fichier)
                    If Mid(Fichier, I, 1) = "\" Then NameFic = Mid(Fichier, I + 1, Len(Fichier) - I)
                Next
            Else
                NameFic = ""
            End If

            NameFicOf = NameFic
        Catch ex As Exception
            Call FichierErreur(ex.Message)
        End Try
    End Function
    Public Shared Function NameFic_Ext(ByVal Fichier As String) As String
        Dim NameFic As String = String.Empty
        Dim K As Integer
        NameFic_Ext = ""
        Try
            If Fichier <> "" Then
                For K = 1 To Len(Fichier)
                    If Mid(Fichier, K, 1) = "\" Then NameFic = Mid(Fichier, K + 1, Len(Fichier) - K)
                Next
                If NameFic <> "" Then Fichier = NameFic
                For K = 1 To Len(Fichier)
                    If Mid(Fichier, K, 1) = "." Then NameFic = Mid(Fichier, 1, K - 1)
                Next
            Else
                NameFic = ""
            End If

            NameFic_Ext = NameFic
        Catch ex As Exception
            Call FichierErreur(ex.Message)
        End Try
    End Function
    Public Shared Function ExtOf(ByVal Fichier As String) As String
        Dim Extension As String = String.Empty
        Dim K As Integer
        ExtOf = ""
        Try
            If Fichier <> "" Then

                For K = 1 To Len(Fichier)
                    If Mid(Fichier, K, 1) = "." Then Extension = Mid(Fichier, K + 1, Len(Fichier) - K)
                Next
            Else
                Extension = ""
            End If

            ExtOf = Extension
        Catch ex As Exception
            Call FichierErreur(ex.Message)
        End Try
    End Function
    Public Shared Sub FichierErreur(ByVal Erreur As String, Optional ByVal Suffixe As String = "", Optional ByVal Suffixe2 As String = "")
        Try

            If Not Directory.Exists(My.Application.Info.DirectoryPath & "\Logs") Then Directory.CreateDirectory(My.Application.Info.DirectoryPath & "\Logs")
            Dim Fic As String = String.Format("{0}_Journal_{1}_{2}.log", My.Application.Info.AssemblyName, Suffixe, Suffixe2)
            Dim SW As StreamWriter = New StreamWriter(My.Application.Info.DirectoryPath & "\Logs\" & Fic, True)

            SW.WriteLine(Now & " - " & Erreur & Chr(13))

            SW.Close()

        Catch ex As Exception
            'MsgBox("Erreur : " & ex.Message)
        End Try
    End Sub
    Public Shared Sub FichierMail(ByVal Texte As String)
        Try
            'If Not Directory.Exists(My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData & "\JVAPP\Logs") Then Directory.CreateDirectory(My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData & "\JVAPP\Logs")
            If Not Directory.Exists(My.Application.Info.DirectoryPath & "\Logs") Then Directory.CreateDirectory(My.Application.Info.DirectoryPath & "\Logs")


            Dim SW As StreamWriter = New StreamWriter(My.Application.Info.DirectoryPath & "\Logs\TxtMail.log", True)

            SW.WriteLine(Now & " - " & Texte & Chr(13))

            SW.Close()

        Catch ex As Exception
            FichierErreur("Erreur : " & ex.Message)
        End Try
    End Sub
    Public Shared Sub EcrireFic(ByVal Path As String, ByVal Fic As String, ByVal Texte As String)
        Try
            If Not Directory.Exists(Path) Then Directory.CreateDirectory(Path)

            Dim SW As StreamWriter = New StreamWriter(Path & Fic, True)

            SW.WriteLine(Now & " - " & Texte & Chr(13))

            SW.Close()

        Catch ex As Exception
            FichierErreur("Erreur : " & ex.Message)
        End Try
    End Sub

    Public Shared Function StringToPourcent(ByVal Chaine As String)
        Dim ValeurChaine As Double = 0
        On Error Resume Next
        If Chaine = "" Then
            StringToPourcent = "0.00 %"
        Else
            If Right(Chaine, 1) = "%" Then
                Chaine = Left(Chaine, Len(Chaine) - 1)
            End If
            ValeurChaine = FormatNumber(Chaine, 4)
            If IsNumeric(ValeurChaine) Then
                StringToPourcent = CStr(ValeurChaine) & " %"
            Else
                StringToPourcent = "0.00 %"
            End If
        End If
        Return StringToPourcent
    End Function
    Public Shared Function IsPercent(ByVal Chaine As String)
        IsPercent = False
        If Chaine <> "" Then
            If Right$(Chaine, 1) = "%" Then Chaine = Left$(Chaine, Len(Chaine) - 1)
            If IsNumeric(Chaine) Then IsPercent = True
        End If
    End Function
    Public Shared Function CNumPercent(ByVal Chaine As String)
        CNumPercent = 0
        If Chaine <> "" Then
            If Right$(Chaine, 1) = "%" Then Chaine = Left$(Chaine, Len(Chaine) - 1)
            If IsNumeric(Chaine) Then CNumPercent = CDbl(Chaine)
        End If
    End Function
    Public Shared Function CNumCurr(ByVal Chaine As String)
        CNumCurr = 0
        If Chaine <> "" Then
            If Not (Asc(Right$(Chaine, 1)) > 47 And Asc(Right$(Chaine, 1))) < 58 Then Chaine = Left$(Chaine, Len(Chaine) - 1)
            If IsNumeric(Chaine) Then CNumCurr = CDbl(Chaine)
        End If
    End Function
    Public Shared Function DotDec(ByVal AConvert As Double)
        If IsNumeric(AConvert) Then
            DotDec = Replace(CStr(AConvert), ",", ".")
        Else
            DotDec = 0
        End If
    End Function

    Public Shared Function NormaliserDate(ByVal dateaverif As String)
        NormaliserDate = String.Empty
        Try
            NormaliserDate = ""

            If Len(dateaverif) = 4 Then
                dateaverif = dateaverif & Mid(CStr(Now), 7, 4)
            End If
            If Len(dateaverif) = 5 Then
                dateaverif = dateaverif & "/" & Mid(CStr(Now), 7, 4)
            End If
            If dateaverif <> "" And Len(dateaverif) <> 10 Then
                If Not IsDate(dateaverif) Then
                    dateaverif = Left(dateaverif, 2) & "/" & Mid(dateaverif, 3, 2) & "/" & Mid(dateaverif, 5, Len(dateaverif) - 4)
                    NormaliserDate = dateaverif
                End If
            End If
            If Len(dateaverif) = 8 Then
                dateaverif = Left(dateaverif, 6) & "20" & Right(dateaverif, 2)
                NormaliserDate = dateaverif
            End If
            NormaliserDate = dateaverif
            If Not IsDate(dateaverif) Then
                dateaverif = ""
                NormaliserDate = dateaverif
            End If
        Catch ex As Exception

        End Try
    End Function
    Public Shared Function SerialiserDate(ByVal DateVerif As String)
        Dim Sep1 As Integer = 0
        Dim Sep2 As Integer = 0
        SerialiserDate = String.Empty
        Try
            SerialiserDate = ""

            If Len(DateVerif) = 4 Then
                DateVerif = DateVerif & Mid(CStr(Now), 7, 4)
            End If
            If Len(DateVerif) = 5 Then
                DateVerif = DateVerif & "/" & Mid(CStr(Now), 7, 4)
            End If
            If DateVerif <> "" And Len(DateVerif) <> 10 Then
                If Not IsDate(DateVerif) Then
                    DateVerif = Left(DateVerif, 2) & "/" & Mid(DateVerif, 3, 2) & "/" & Mid(DateVerif, 5, Len(DateVerif) - 4)
                    SerialiserDate = DateVerif
                End If
            End If
            If Len(DateVerif) = 8 Then
                DateVerif = Left(DateVerif, 6) & "20" & Right(DateVerif, 2)
                SerialiserDate = DateVerif
            End If
            SerialiserDate = DateVerif
            If Not IsDate(DateVerif) Then
                DateVerif = "01/01/2000"
                SerialiserDate = DateVerif
            End If
            DateVerif = SerialiserDate
            SerialiserDate = Mid(DateVerif, 7, 4) & Mid(DateVerif, 4, 2) & Mid(DateVerif, 1, 2)

        Catch ex As Exception

        End Try
    End Function
    Public Shared Function Encodage(ByVal Texte As String)
        'MsgBox(System.Text.ASCIIEncoding.ASCII.GetString(System.Text.Encoding.ASCII.GetBytes(Texte)))
        Encodage = System.Text.Encoding.ASCII.GetString(System.Text.ASCIIEncoding.Convert(System.Text.Encoding.Default, System.Text.Encoding.Default, System.Text.Encoding.ASCII.GetBytes(Texte)))
        'System.Text.UnicodeEncoding.
    End Function
    Public Shared Function CodeAnsi(ByVal Texte As String)


        Texte = Replace(Texte, "à", "a")
        Texte = Replace(Texte, "ä", "a")
        Texte = Replace(Texte, "â", "a")
        Texte = Replace(Texte, "é", "e")
        Texte = Replace(Texte, "è", "e")
        Texte = Replace(Texte, "ê", "e")
        Texte = Replace(Texte, "ë", "e")
        Texte = Replace(Texte, "î", "i")
        Texte = Replace(Texte, "ï", "i")
        Texte = Replace(Texte, "ô", "o")
        Texte = Replace(Texte, "ö", "o")
        Texte = Replace(Texte, "û", "u")
        Texte = Replace(Texte, "ü", "u")
        Texte = Replace(Texte, "ù", "u")
        Texte = Replace(Texte, "ÿ", "y")
        CodeAnsi = Replace(Texte, "ç", "c")

    End Function
    Public Shared Function CarImprimables(ByVal Texte As String) As String
        CarImprimables = ""
        Try
            If Texte <> "" Then
                For I As Integer = 1 To Len(Texte)
                    Dim J As Integer = Asc(Mid(Texte, I, 1))
                    If (J > 47 And J < 58) Or (J > 64 And J < 91) Or (J > 96 And J < 123) Then CarImprimables = CarImprimables & Chr(J)
                    'MsgBox(CarImprimables)
                Next
            End If
        Catch ex As Exception

        End Try
    End Function
    Public Shared Function NormaliserNum(ByVal MntAVerif As String)
        Dim I As Integer

        For I = 1 To Len(MntAVerif)
            If Mid(MntAVerif, I, 1) = "." Then Mid(MntAVerif, I, 1) = ","
        Next I
        If IsNumeric(MntAVerif) Then
            NormaliserNum = FormatCurrency(MntAVerif)
        Else
            NormaliserNum = ""
        End If
    End Function
    Public Shared Function NormaliserCur2(ByVal MntAVerif As String)
        Dim I As Integer

        For I = 1 To Len(MntAVerif)
            If Mid(MntAVerif, I, 1) = "." Then Mid(MntAVerif, I, 1) = ","
        Next I
        If IsNumeric(MntAVerif) Then
            NormaliserCur2 = FormatCurrency(MntAVerif, 2)
        Else
            NormaliserCur2 = ""
        End If
    End Function
    Public Shared Function HexToDec(ByVal Numero As String)
        Dim HexAA As String = String.Empty
        Dim HexBB As String = String.Empty
        Dim NumAA As Integer = 0
        Dim NumBB As Integer = 0

        If Numero <> "" Then
            HexAA = Right(Numero, 1)
            HexBB = Left(Numero, 1)
            If Asc(HexAA) > 50 Then
                NumAA = Asc(HexAA) - 55
            Else
                NumAA = Asc(HexAA) - 48
            End If
            If Asc(HexBB) > 50 Then
                NumBB = Asc(HexBB) - 55
            Else
                NumBB = Asc(HexBB) - 48
            End If
            HexToDec = Str(NumBB * 16 + NumAA)
        Else
            HexToDec = "00"
        End If
    End Function
    Public Shared Function TrimVal(ByVal Chaine As String)
        Dim CaractChaine As String = String.Empty

        If Chaine <> "" Then
            CaractChaine = ""
            For I = 1 To Len(Chaine)
                If Asc(Mid(Chaine, I, 1)) > 43 And Asc(Mid(Chaine, I, 1)) < 58 Then
                    Select Case Asc(Mid(Chaine, I, 1))
                        Case 44
                            Mid(Chaine, I, 1) = "."
                        Case 46
                            Mid(Chaine, I, 1) = "."
                    End Select
                    CaractChaine = CaractChaine & Mid(Chaine, I, 1)
                End If
            Next I
            TrimVal = Val(CaractChaine)
        Else
            TrimVal = 0
        End If
    End Function
    Public Shared Function NumDate(ByVal datetest As String)
        If IsDate(datetest) Then
            NumDate = CDate(datetest) - CDate("30/12/1899")
        Else
            NumDate = 0
        End If
    End Function
    Public Shared Function FirstUcase(ByVal TexteAConvertir As String)
        FirstUcase = ""
        If TexteAConvertir <> "" Then
            FirstUcase = UCase(Left(TexteAConvertir, 1)) & Right(TexteAConvertir, Len(TexteAConvertir) - 1)
        End If
    End Function
    Public Shared Function FirstUcaseMultiple(ByVal TexteAConvertir As String)
        Dim J As Integer
        FirstUcaseMultiple = ""
        Select Case Len(TexteAConvertir)
            Case 0
                FirstUcaseMultiple = ""
            Case 1
                FirstUcaseMultiple = UCase(TexteAConvertir)
                FirstUcaseMultiple = LTrim(FirstUcaseMultiple)
            Case Is > 1
                TexteAConvertir = LCase(TexteAConvertir)
                FirstUcaseMultiple = UCase(Mid$(TexteAConvertir, 1, 1))
                For J = 1 To Len(TexteAConvertir) - 1
                    If Asc(Mid$(TexteAConvertir, J, 1)) = 32 Then
                        FirstUcaseMultiple = FirstUcaseMultiple & UCase(Mid$(TexteAConvertir, J + 1, 1))
                    Else
                        FirstUcaseMultiple = FirstUcaseMultiple & Mid$(TexteAConvertir, J + 1, 1)
                    End If
                Next J
                FirstUcaseMultiple = LTrim(FirstUcaseMultiple)
                FirstUcaseMultiple = RTrim(FirstUcaseMultiple)
        End Select
    End Function


    'public shared Sub EnvoiMail(ByVal Sujet As String)
    '    Dim NewMail As New MailMessage()
    '    Dim smtpServer As New System.Net.Mail.SmtpClient
    '    Dim Exp As String = String.Empty
    '    Dim SMTPName As String = String.Empty
    '    Dim MailUser As String = String.Empty
    '    Dim MailPwd As String = String.Empty
    '    Dim SmtpPort As String = String.Empty
    '    Dim Dest As String = String.Empty
    '    Dim Copy As String = String.Empty
    '    Dim IsSec As Boolean = False
    '    Dim IsAuth As Boolean = False
    '    Dim TexteMail As String = String.Empty
    '    Dim Connect As New OleDb.OleDbConnection
    '    Dim Cmd As New OleDb.OleDbCommand
    '    Dim ExistsMail As Boolean = False
    '    Try

    '        If Main.BaseTravail <> "" Then
    '            Connect.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Main.BaseTravail
    '            Connect.Open()
    '            Cmd.Connection() = Connect
    '            Cmd.CommandText = "SELECT M_Exp, M_SmtpName, M_MailUser, M_MailPwd, M_SmtpPort, M_Dest, M_DestCopy, M_SSL, M_Auth From F_Mail "
    '            Dim myReader As OleDb.OleDbDataReader = Cmd.ExecuteReader()
    '            Do While myReader.Read
    '                If Not IsDBNull(myReader.GetString(0)) Then Exp = myReader.GetString(0)
    '                If Not IsDBNull(myReader.GetString(1)) Then SMTPName = myReader.GetString(1)
    '                If Not IsDBNull(myReader.GetString(2)) Then MailUser = myReader.GetString(2)
    '                If Not IsDBNull(myReader.GetString(3)) Then MailPwd = DecodePwd(myReader.GetString(3))
    '                If Not IsDBNull(myReader.GetString(4)) Then SmtpPort = myReader.GetString(4)
    '                If Not IsDBNull(myReader.GetString(5)) Then Dest = myReader.GetString(5)
    '                If Not IsDBNull(myReader.GetString(6)) Then Copy = myReader.GetString(6)
    '                If Not IsDBNull(myReader.GetValue(7)) Then
    '                    If myReader.GetInt16(7) = 1 Then
    '                        IsSec = True
    '                    Else
    '                        IsSec = False
    '                    End If
    '                End If
    '                If Not IsDBNull(myReader.GetValue(8)) Then
    '                    If myReader.GetInt16(8) = 1 Then
    '                        IsAuth = True
    '                    Else
    '                        IsAuth = False
    '                    End If
    '                End If
    '            Loop
    '            myReader.Close()

    '            Cmd = Nothing
    '            Connect.Close()
    '        Else
    '            MsgBox("Il faut choisir une base de travail", MsgBoxStyle.OkOnly)
    '        End If
    '        If File.Exists(Application.StartupPath & "\Logs\TxtMail.log") Then
    '            Dim SR As StreamReader = New StreamReader(Application.StartupPath & "\Logs\TxtMail.log")
    '            ExistsMail = True
    '            TexteMail = SR.ReadToEnd
    '            SR.Close()
    '            'File.Delete(Application.StartupPath & "\Logs\TxtMail.log")
    '            TexteMail = TexteMail & Chr(13)
    '            TexteMail = TexteMail & "----------------------------------------------------------------"
    '            TexteMail = TexteMail & Chr(13)
    '            TexteMail = TexteMail & "Message automatique envoyé par intégration EDI"
    '        Else
    '            TexteMail = "Aucune Commande Transmise"

    '            TexteMail = TexteMail & Chr(13)
    '            TexteMail = TexteMail & "----------------------------------------------------------------"
    '            TexteMail = TexteMail & Chr(13)
    '            TexteMail = TexteMail & "Message automatique envoyé par intégration EDI"
    '        End If
    '        If IsAuth Then smtpServer.Credentials = New Net.NetworkCredential(MailUser, MailPwd)
    '        smtpServer.Port = SmtpPort
    '        smtpServer.Host = SMTPName

    '        If Dest <> "" Then NewMail.To.Add(Dest)
    '        'newmail.To.Add("G2L2@wanadoo.fr")
    '        If Copy <> "" Then NewMail.CC.Add(Copy)
    '        NewMail.IsBodyHtml = False

    '        NewMail.From = New System.Net.Mail.MailAddress(Exp)
    '        NewMail.Subject = Sujet
    '        NewMail.Body = TexteMail
    '        NewMail.Sender = New System.Net.Mail.MailAddress(Exp)

    '        'Dim PJ As Net.Mail.Attachment = New Net.Mail.Attachment(GetFolderPath(SpecialFolder.MyDocuments) & "\Logs\TxtMail.log")
    '        'newmail.Attachments.Add(PJ)
    '        If IsSec Then smtpServer.EnableSsl = True
    '        If ExistsMail And Dest <> "" Then smtpServer.Send(NewMail)

    '    Catch ex As Exception
    '        FichierErreur(ex.Message)
    '    End Try

    'End Sub
    Public Shared Function EnvoiMail(ByVal Sujet As String, ByVal Texte As String) As String
        Dim NewMail As New MailMessage()
        Dim smtpServer As New System.Net.Mail.SmtpClient

        Dim IsSec As Boolean = False
        Dim IsAuth As Boolean = False
        EnvoiMail = String.Empty


        Try

            If ParamsMail.AUTH = "1" Then smtpServer.Credentials = New Net.NetworkCredential(ParamsMail.SenderUser, ParamsMail.SenderPwd)
            If ParamsMail.SMTPPort <> "" Then smtpServer.Port = CInt(ParamsMail.SMTPPort)
            smtpServer.Host = ParamsMail.SmtpServer

            If ParamsMail.Dest <> "" Then NewMail.To.Add(ParamsMail.Dest)
            If ParamsMail.DestCopy <> "" Then NewMail.CC.Add(ParamsMail.DestCopy)

            NewMail.IsBodyHtml = False
            NewMail.From = New System.Net.Mail.MailAddress(ParamsMail.Sender)
            NewMail.Subject = Sujet
            NewMail.Body = Texte
            NewMail.Sender = New System.Net.Mail.MailAddress(ParamsMail.Sender)


            If ParamsMail.SSL = "1" Then smtpServer.EnableSsl = True
            smtpServer.Send(NewMail)
            EnvoiMail = "Mail Envoyé"
        Catch ex As Exception
            'MsgBox(ex.Message)
            FichierErreur("Commun.EnvoiMail " & ex.Message)
        Finally
            If EnvoiMail = "" Then EnvoiMail = "Erreur dans l'envoi du mail"
        End Try

    End Function
    Public Shared Function BaseSqlFic(ByVal Fichier As String) As String
        BaseSqlFic = String.Empty
        If File.Exists(Fichier) Then
            Dim Hash() = Split(Fichier, "\")
            BaseSqlFic = Hash(Hash.Length - 1)
            BaseSqlFic = Mid(BaseSqlFic, 1, Len(BaseSqlFic) - 4)
        End If
    End Function
    Public Shared Function InstanceSqlFic(ByVal Fichier As String) As String
        InstanceSqlFic = String.Empty
        If File.Exists(Fichier) Then
            Dim SR As StreamReader = New StreamReader(Fichier)
            Do Until SR.Peek = -1
                Dim LigneLue As String = SR.ReadLine
                If Mid(LigneLue, 1, 11) = "ServeurSQL=" Then InstanceSqlFic = Mid(LigneLue, 12, Len(LigneLue) - 11)
            Loop
            SR.Close()
        End If
    End Function
    Public Shared Function IsRecordLock(ByVal Table As String, ByVal CbMarq As String, ByVal cnx As SqlClient.SqlConnection) As Boolean
        IsRecordLock = False
        Dim CmdSQL As SqlClient.SqlCommand = New SqlClient.SqlCommand
        Dim StrSql As String = String.Empty
        Try
            If CbMarq <> "" Then
                CmdSQL.Connection = cnx

                StrSql = " declare @lRes int exec dbo.CB_IsRecordLock '{0}', {1}, @lRes OUTPUT select @lRes as Used"
                CmdSQL.CommandText = String.Format(StrSql, Table, CbMarq)
                If CmdSQL.ExecuteScalar() = 1 Then IsRecordLock = True
            End If
            CmdSQL.Dispose()
            CmdSQL = Nothing

        Catch ex As Exception
            'MsgBox(ex.Message)
            Call FJV.FichierErreur(ex.Message & " - ")
        End Try
    End Function
    Public Shared Function LockRecord(ByVal Table As String, ByVal CbMarq As String, ByVal cnx As SqlClient.SqlConnection) As Boolean
        LockRecord = False
        Dim CmdSQL As SqlClient.SqlCommand = New SqlClient.SqlCommand
        Dim StrSql As String = String.Empty
        Try
            If CbMarq <> "" Then
                CmdSQL.Connection = cnx

                StrSql = "exec dbo.CB_LockRecord '{0}', 3,{1} "
                CmdSQL.CommandText = String.Format(StrSql, Table, CbMarq)
                CmdSQL.ExecuteNonQuery()
            End If
            CmdSQL.Dispose()
            CmdSQL = Nothing

        Catch ex As Exception
            'MsgBox(ex.Message)
            Call FJV.FichierErreur(ex.Message & " - ")
        End Try
    End Function
    Public Shared Function UnLockRecord(ByVal Table As String, ByVal CbMarq As String, ByVal cnx As SqlClient.SqlConnection) As Boolean
        UnLockRecord = False
        Dim CmdSQL As SqlClient.SqlCommand = New SqlClient.SqlCommand
        Dim StrSql As String = String.Empty
        Try
            If CbMarq <> "" Then
                CmdSQL.Connection = cnx

                StrSql = "exec dbo.CB_UnLockRecord '{0}', 3,{1} "
                CmdSQL.CommandText = String.Format(StrSql, Table, CbMarq)
                CmdSQL.ExecuteNonQuery()
            End If
            CmdSQL.Dispose()
            CmdSQL = Nothing

        Catch ex As Exception
            'MsgBox(ex.Message)
            Call FJV.FichierErreur(ex.Message & " - ")
        End Try

    End Function
    Public Shared Function StSql(ByVal Texte As String) As String
        If Texte <> "" Then StSql = Replace(Trim(Texte), "'", "''") Else StSql = ""
    End Function
    Public Shared Function NumSql(ByVal Chiffre As String, Optional ByVal NbDecimales As Integer = -1) As String
        Dim Sep As String = Mid(CStr(5 / 2), 2, 1)
        Dim ValeurChiffre As String = Replace(Replace(Chiffre, ",", Sep), ".", Sep)

        If IsNumeric(ValeurChiffre) Then
            If NbDecimales > -1 Then
                NumSql = Replace(CStr(FormatNumber(ValeurChiffre, NbDecimales, TriState.True, TriState.False, TriState.False)), ",", ".")
            Else
                NumSql = Replace(CStr(ValeurChiffre), ",", ".")
            End If
        Else
            NumSql = "0"
        End If
    End Function
    Public Shared Function NumSqlNum(ByVal Chiffre As String) As Double

        Dim Sep As String = Mid(CStr(5 / 2), 2, 1)

        If Chiffre <> "" Then
            Select Case Sep
                Case Is = "."
                    If IsNumeric(Replace(CStr(Chiffre), ",", ".")) Then NumSqlNum = CDbl(Replace(CStr(Chiffre), ",", ".")) Else NumSqlNum = 0
                Case Is = ","
                    If IsNumeric(Replace(CStr(Chiffre), ".", ",")) Then NumSqlNum = CDbl(Replace(CStr(Chiffre), ".", ",")) Else NumSqlNum = 0
            End Select

        Else
            NumSqlNum = 0
        End If
    End Function
    Public Shared Function StXML(ByVal Texte As String) As String
        StXML = String.Empty
        If Texte <> "" Then StXML = Replace(Texte, "&", "&amp;")
        If StXML <> "" Then StXML = Replace(StXML, "<", "&lt;")
        If StXML <> "" Then StXML = Replace(StXML, ">", "&gt;")
        If StXML <> "" Then StXML = Replace(StXML, """", "&quot;")
        If StXML <> "" Then StXML = Replace(StXML, "'", "&apos;")
    End Function
    Public Shared Function XMLSt(ByVal Texte As String) As String
        XMLSt = String.Empty
        If Texte <> "" Then XMLSt = Replace(Texte, "&amp;", "&")
        If XMLSt <> "" Then XMLSt = Replace(XMLSt, "&lt;", "<")
        If XMLSt <> "" Then XMLSt = Replace(XMLSt, "&gt;", ">")
        If XMLSt <> "" Then XMLSt = Replace(XMLSt, "&quot;", """")
        If XMLSt <> "" Then XMLSt = Replace(XMLSt, "&apos;", "'")
    End Function
    Public Shared Function EnvoiServiceMail(ByVal Expediteur As String, _
                                     ByVal Destinataire As String, _
                                     ByVal DestinataireCopy As String, _
                                     ByVal Sujet As String, _
                                     ByVal TexteMail As String, _
                                     ByVal PieceJointe As String, _
                                     ByVal IsHtml As Boolean, _
                                    ByVal PathServiceMail As String) As Boolean
        EnvoiServiceMail = False

        Try
            Dim Fichier As String = PathServiceMail & "\" & Now.Year & Now.DayOfYear & Now.Hour & Now.Minute & Now.Second & Now.Millisecond & ".xml"
            Dim MailXml As XmlTextWriter = New XmlTextWriter(Fichier, System.Text.Encoding.UTF8)
            MailXml.Formatting = System.Xml.Formatting.Indented
            MailXml.WriteStartDocument(False)
            MailXml.WriteComment("Mail pour le service Mail")
            'Crée l'élément de document principal.
            MailXml.WriteStartElement("Mail")


            MailXml.WriteElementString("Exp", Expediteur)
            MailXml.WriteElementString("Dest", Destinataire)
            MailXml.WriteElementString("Dest_Copy", DestinataireCopy)
            MailXml.WriteElementString("Sujet", Sujet)
            MailXml.WriteElementString("Texte", TexteMail)
            If IsHtml Then MailXml.WriteElementString("HTML", 1) Else MailXml.WriteElementString("HTML", 0)

            If PieceJointe <> "" Then
                For Each Piece As String In Split(PieceJointe, ";")
                    MailXml.WriteStartElement("PieceJointe")
                    MailXml.WriteElementString("NomFichier", Piece)
                    MailXml.WriteEndElement()
                Next
            End If

            MailXml.WriteEndElement()
            MailXml.Flush()
            MailXml.Close()
            EnvoiServiceMail = True

        Catch ex As Exception
            FichierErreur(ex.Message)
            FJV.FichierErreur("EnvoiServiceMail " & ex.Message)
        End Try

    End Function
    Public Shared Function EANT48L(ByVal EAN13 As String) As String
        EANT48L = String.Empty
        If Len(EAN13) < 12 Or Len(EAN13) > 13 Then Exit Function
        Dim i As Integer = 0, Séquence As String = String.Empty, Clé As Integer = 0
        Dim Facteur As Integer = 0, Total As Integer = 0
        'Caractère de début + séparateur
        Select Case Mid(EAN13, 1, 1)
            Case 0
                EANT48L = "#:"
                Séquence = "000000"
            Case 1
                EANT48L = "$:"
                Séquence = "001011"
            Case 2
                EANT48L = "%:"
                Séquence = "001101"
            Case 3
                EANT48L = "&:"
                Séquence = "001110"
            Case 4
                EANT48L = "(:"
                Séquence = "010011"
            Case 5
                EANT48L = "):"
                Séquence = "011001"
            Case 6
                EANT48L = "*:"
                Séquence = "011100"
            Case 7
                EANT48L = "+:"
                Séquence = "010101"
            Case 8
                EANT48L = ",:"
                Séquence = "010110"
            Case 9
                EANT48L = "-:"
                Séquence = "011010"
            Case Else
                FichierErreur("Ereur de la macro de transcription EAN13", vbOKOnly + vbCritical, "Erreur")
        End Select
        'Transcription de la première partie du code
        For i = 2 To 7
            Select Case Mid(Séquence, i - 1, 1)
                Case 0
                    Select Case Mid(EAN13, i, 1)
                        Case 0
                            EANT48L = EANT48L & "A"
                        Case 1
                            EANT48L = EANT48L & "B"
                        Case 2
                            EANT48L = EANT48L & "C"
                        Case 3
                            EANT48L = EANT48L & "D"
                        Case 4
                            EANT48L = EANT48L & "E"
                        Case 5
                            EANT48L = EANT48L & "F"
                        Case 6
                            EANT48L = EANT48L & "G"
                        Case 7
                            EANT48L = EANT48L & "H"
                        Case 8
                            EANT48L = EANT48L & "I"
                        Case 9
                            EANT48L = EANT48L & "J"
                    End Select
                Case 1
                    Select Case Mid(EAN13, i, 1)
                        Case 0
                            EANT48L = EANT48L & "K"
                        Case 1
                            EANT48L = EANT48L & "L"
                        Case 2
                            EANT48L = EANT48L & "M"
                        Case 3
                            EANT48L = EANT48L & "N"
                        Case 4
                            EANT48L = EANT48L & "O"
                        Case 5
                            EANT48L = EANT48L & "P"
                        Case 6
                            EANT48L = EANT48L & "Q"
                        Case 7
                            EANT48L = EANT48L & "R"
                        Case 8
                            EANT48L = EANT48L & "S"
                        Case 9
                            EANT48L = EANT48L & "T"
                    End Select
                Case Else
                    FichierErreur("Erreur de Séquence", vbCritical + vbOKOnly, "Erreur")
            End Select
        Next
        'Caractère de séparation des deux parties
        EANT48L = EANT48L & "="
        For i = 8 To 12
            Select Case Mid(EAN13, i, 1)
                Case 0
                    EANT48L = EANT48L & "U"
                Case 1
                    EANT48L = EANT48L & "V"
                Case 2
                    EANT48L = EANT48L & "W"
                Case 3
                    EANT48L = EANT48L & "X"
                Case 4
                    EANT48L = EANT48L & "Y"
                Case 5
                    EANT48L = EANT48L & "Z"
                Case 6
                    EANT48L = EANT48L & "["
                Case 7
                    EANT48L = EANT48L & "\"
                Case 8
                    EANT48L = EANT48L & "]"
                Case 9
                    EANT48L = EANT48L & "^"
            End Select
        Next
        'Vérification de la clé
        If Len(EAN13) < 13 Then EAN13 = StrDup(13 - Len(EAN13), "0") & EAN13
        EAN13 = Mid(Trim(EAN13), 1, 12)
        Facteur = 3
        For i = Len(EAN13) To 1 Step -1
            Total = Total + Mid(EAN13, i, 1) * Facteur
            Facteur = 4 - Facteur
        Next i
        Clé = 10 - IIf(Total Mod 10 <> 0, Total Mod 10, 10)
        Select Case Clé
            Case 0
                EANT48L = EANT48L & "U:"
            Case 1
                EANT48L = EANT48L & "V:"
            Case 2
                EANT48L = EANT48L & "W:"
            Case 3
                EANT48L = EANT48L & "X:"
            Case 4
                EANT48L = EANT48L & "Y:"
            Case 5
                EANT48L = EANT48L & "Z:"
            Case 6
                EANT48L = EANT48L & "[:"
            Case 7
                EANT48L = EANT48L & "\:"
            Case 8
                EANT48L = EANT48L & "]:"
            Case 9
                EANT48L = EANT48L & "^:"
        End Select
    End Function

    Public Shared Function Code128(ByVal Chaine As String) As String
        'V 2.0.0
        'Paramètres : une chaine
        'Parameters : a string
        'Retour : * une chaine qui, affichée avec la police CODE128.TTF, donne le code barre
        '         * une chaine vide si paramètre fourni incorrect
        'Return : * a string which give the bar code when it is dispayed with CODE128.TTF font
        '         * an empty string if the supplied parameter is no good
        Dim i%, checksum&, mini%, dummy%
        Dim tableB As Boolean

        Code128 = ""
        If Len(Chaine$) > 0 Then
            'Vérifier si caractères valides
            'Check for valid characters
            For i% = 1 To Len(Chaine$)
                Select Case Asc(Mid$(Chaine$, i%, 1))
                    Case 32 To 126, 203
                    Case Else
                        i% = 0
                        Exit For
                End Select
            Next
            'Calculer la chaine de code en optimisant l'usage des tables B et C
            'Calculation of the code string with optimized use of tables B and C
            Code128$ = ""
            tableB = True
            If i% > 0 Then
                i% = 1 'i% devient l'index sur la chaine / i% become the string index
                Do While i% <= Len(Chaine$)
                    If tableB Then
                        'Voir si intéressant de passer en table C / See if interesting to switch to table C
                        'Oui pour 4 chiffres au début ou à la fin, sinon pour 6 chiffres / yes for 4 digits at start or end, else if 6 digits
                        mini% = IIf(i% = 1 Or i% + 3 = Len(Chaine$), 4, 6)

                        mini = TestNum(Chaine, i, mini)

                        If mini% < 0 Then 'Choix table C / Choice of table C
                            If i% = 1 Then 'Débuter sur table C / Starting with table C
                                Code128$ = Chr(210)
                            Else 'Commuter sur table C / Switch to table C
                                Code128$ = Code128$ & Chr(204)
                            End If
                            tableB = False
                        Else
                            If i% = 1 Then Code128$ = Chr(209) 'Débuter sur table B / Starting with table B
                        End If
                    End If
                    If Not tableB Then
                        'On est sur la table C, essayer de traiter 2 chiffres / We are on table C, try to process 2 digits
                        mini% = 2
                        mini = TestNum(Chaine, i, mini)
                        If mini% < 0 Then 'OK pour 2 chiffres, les traiter / OK for 2 digits, process it
                            dummy% = Val(Mid$(Chaine$, i%, 2))
                            dummy% = IIf(dummy% < 95, dummy% + 32, dummy% + 105)
                            Code128$ = Code128$ & Chr(dummy%)
                            i% = i% + 2
                        Else 'On n'a pas 2 chiffres, repasser en table B / We haven't 2 digits, switch to table B
                            Code128$ = Code128$ & Chr(205)
                            tableB = True
                        End If
                    End If
                    If tableB Then
                        'Traiter 1 caractère en table B / Process 1 digit with table B
                        Code128$ = Code128$ & Mid$(Chaine$, i%, 1)
                        i% = i% + 1
                    End If
                Loop
                'Calcul de la clé de contrôle / Calculation of the checksum
                For i% = 1 To Len(Code128$)
                    dummy% = Asc(Mid$(Code128$, i%, 1))
                    dummy% = IIf(dummy% < 127, dummy% - 32, dummy% - 105)
                    If i% = 1 Then checksum& = dummy%
                    checksum& = (checksum& + (i% - 1) * dummy%) Mod 103
                Next
                'Calcul du code ASCII de la clé / Calculation of the checksum ASCII code
                checksum& = IIf(checksum& < 95, checksum& + 32, checksum& + 105)
                'Ajout de la clé et du STOP / Add the checksum and the STOP
                Code128$ = Code128$ & Chr(checksum&) & Chr(211)
            End If
        End If
        Exit Function

    End Function

    Public Shared Function TestNum(ByVal Chaine As String, ByVal I As Integer, ByVal Mini As Integer) As Integer
        TestNum = Mini
        Mini% = Mini% - 1
        If I% + Mini% <= Len(Chaine$) Then
            Do While Mini% >= 0
                If Asc(Mid$(Chaine$, I% + Mini%, 1)) < 48 Or Asc(Mid$(Chaine$, I% + Mini%, 1)) > 57 Then Exit Do
                Mini% = Mini% - 1
            Loop
        End If
        TestNum = Mini
    End Function

    Public Shared Function SSCC(ByVal Chaine As String) As String
        'V 2.0.0
        'Paramètres : une chaine
        'Parameters : a string
        'Retour : * une chaine qui, affichée avec la police CODE128.TTF, donne le code barre
        '         * une chaine vide si paramètre fourni incorrect
        'Return : * a string which give the bar code when it is dispayed with CODE128.TTF font
        '         * an empty string if the supplied parameter is no good
        Dim i%, checksum&, mini%, dummy%
        Dim tableB As Boolean
        Chaine = AddCheckDigit(Chaine)
        SSCC = ""
        If Len(Chaine$) > 0 Then
            'Vérifier si caractères valides
            'Check for valid characters
            For i% = 1 To Len(Chaine$)
                Select Case Asc(Mid$(Chaine$, i%, 1))
                    Case 32 To 126, 203
                    Case Else
                        i% = 0
                        Exit For
                End Select
            Next
            'Calculer la chaine de code en optimisant l'usage des tables B et C
            'Calculation of the code string with optimized use of tables B and C
            SSCC = ""
            tableB = True
            If i% > 0 Then
                i% = 1 'i% devient l'index sur la chaine / i% become the string index
                Do While i% <= Len(Chaine$)
                    If tableB Then
                        'Voir si intéressant de passer en table C / See if interesting to switch to table C
                        'Oui pour 4 chiffres au début ou à la fin, sinon pour 6 chiffres / yes for 4 digits at start or end, else if 6 digits
                        mini% = IIf(i% = 1 Or i% + 3 = Len(Chaine$), 4, 6)

                        mini = TestNum(Chaine, i, mini)

                        If mini% < 0 Then 'Choix table C / Choice of table C
                            If i% = 1 Then 'Débuter sur table C / Starting with table C
                                SSCC = Chr(210)
                            Else 'Commuter sur table C / Switch to table C
                                SSCC = SSCC & Chr(204)
                            End If
                            tableB = False
                        Else
                            If i% = 1 Then SSCC = Chr(209) 'Débuter sur table B / Starting with table B
                        End If
                    End If
                    If Not tableB Then
                        'On est sur la table C, essayer de traiter 2 chiffres / We are on table C, try to process 2 digits
                        mini% = 2
                        mini = TestNum(Chaine, i, mini)
                        If mini% < 0 Then 'OK pour 2 chiffres, les traiter / OK for 2 digits, process it
                            dummy% = Val(Mid$(Chaine$, i%, 2))
                            dummy% = IIf(dummy% < 95, dummy% + 32, dummy% + 105)
                            SSCC = SSCC & Chr(dummy%)
                            i% = i% + 2
                        Else 'On n'a pas 2 chiffres, repasser en table B / We haven't 2 digits, switch to table B
                            SSCC = SSCC & Chr(205)
                            tableB = True
                        End If
                    End If
                    If tableB Then
                        'Traiter 1 caractère en table B / Process 1 digit with table B
                        SSCC = SSCC & Mid$(Chaine$, i%, 1)
                        i% = i% + 1
                    End If
                Loop
                'Calcul de la clé de contrôle / Calculation of the checksum
                For i% = 1 To Len(SSCC)
                    dummy% = Asc(Mid$(SSCC, i%, 1))
                    dummy% = IIf(dummy% < 127, dummy% - 32, dummy% - 105)
                    If i% = 1 Then checksum& = dummy%
                    checksum& = (checksum& + (i% - 1) * dummy%) Mod 103
                Next
                'Calcul du code ASCII de la clé / Calculation of the checksum ASCII code
                checksum& = IIf(checksum& < 95, checksum& + 32, checksum& + 105)
                'Ajout de la clé et du STOP / Add the checksum and the STOP
                SSCC = SSCC & Chr(checksum&) & Chr(211)
            End If
        End If
        Exit Function

    End Function

    Public Shared Function AddCheckDigit(ByVal Code128 As String) As String
        AddCheckDigit = String.Empty
        Dim CheckSum, A, B, C As Integer
        A = 0
        B = 0
        C = 0

        Dim Coeff As Integer = 1
        For I As Integer = Len(Code128) To 1 Step -1
            Select Case Coeff
                Case Is = 1
                    Coeff = 3
                Case Is = 3
                    Coeff = 1
            End Select
            If IsNumeric(Mid(Code128, I, 1)) Then
                CheckSum += CInt(Mid(Code128, I, 1)) * Coeff
            End If
        Next
        'CheckSum = (Math.Ceiling(CheckSum / 10)) * 10 - CheckSum

        A = Math.Ceiling(CheckSum / 10)
        B = A * 10
        C = B - CheckSum



        'CheckSum = (Fix(CheckSum / 10) + 1) * 10 - CheckSum
        AddCheckDigit = Code128 & CStr(C)

    End Function

    Public Shared Function DateSage(ByVal StrDate As String) As String
        DateSage = String.Empty
        DateSage = Mid(StrDate, 7, 4) & "-" & Mid(StrDate, 4, 2) & "-" & Mid(StrDate, 1, 2)
    End Function
    Public Shared Function NumSage(ByVal Montant As Double) As String
        NumSage = String.Empty
        NumSage = Replace(CStr(FormatNumber(Montant, 2, TriState.True, TriState.False, TriState.False)), ",", ".")
    End Function
    Public Shared Function NumSageABS(ByVal Montant As Double) As String
        NumSageABS = String.Empty
        NumSageABS = Replace(CStr(FormatNumber(Math.Abs(Montant), 2, TriState.True, TriState.False, TriState.False)), ",", ".")
    End Function
    Public Shared Function TypeDocSage(ByVal Id As Integer, Optional ByVal Provenance As Integer = 0) As String

        TypeDocSage = String.Empty
        Select Case Id
            Case Is = 0
                TypeDocSage = "Devis"
            Case Is = 1
                TypeDocSage = "Bon de commande"
            Case Is = 2
                TypeDocSage = "Préparation de livraison"
            Case Is = 3
                TypeDocSage = "Bon de livraison"
            Case Is = 4
                TypeDocSage = "Bon de retour"
            Case Is = 5
                TypeDocSage = "Bon d’avoir"
            Case Is = 6
                If Provenance = 0 Then TypeDocSage = "Facture"
                If Provenance = 1 Then TypeDocSage = "Facture de retour"
                If Provenance = 2 Then TypeDocSage = "Facture d'avoir"
            Case Is = 7
                If Provenance = 0 Then TypeDocSage = "Facture"
                If Provenance = 1 Then TypeDocSage = "Facture de retour"
                If Provenance = 2 Then TypeDocSage = "Facture d'avoir"
            Case Is = 8
                TypeDocSage = "Archive"
            Case Is = 11
                TypeDocSage = "Préparation de commande"
            Case Is = 12
                TypeDocSage = "Bon de Commande"
            Case Is = 13
                TypeDocSage = "Bon de livraison"
            Case Is = 14
                TypeDocSage = "Bon de retour"
            Case Is = 15
                TypeDocSage = "Bon d’avoir"
            Case Is = 16
                TypeDocSage = "Facture"
            Case Is = 17
                TypeDocSage = "Facture comptabilisée"
            Case Is = 18
                TypeDocSage = "Archive"
            Case Is = 20
                TypeDocSage = "Mouvement d’entrée"
            Case Is = 21
                TypeDocSage = "Mouvement de sortie"
            Case Is = 22
                TypeDocSage = "Dépréciation de stock"
            Case Is = 23
                TypeDocSage = "Virement de dépôt à dépôt"
            Case Is = 24
                TypeDocSage = "Préparation de fabrication"
            Case Is = 25
                TypeDocSage = "Ordre de fabrication"
            Case Is = 26
                TypeDocSage = "Bon de fabrication"
            Case Is = 27
                TypeDocSage = "Archive"
            Case Is = 40
                TypeDocSage = "Entrée en stock"
            Case Is = 41
                TypeDocSage = "Sortie de stock"
            Case Is = 42
                TypeDocSage = "Gain financier"
            Case Is = 43
                TypeDocSage = "Perte financière"
        End Select

    End Function

    Public Shared Function UPC_ACarolina(ByVal CodeB As String) As String
        UPC_ACarolina = String.empty
        Try
            Select Case Len(CodeB)
                Case Is > 12
                    UPC_ACarolina = "Trop Long"
                Case Is < 11
                    UPC_ACarolina = "Trop court"
            End Select
            If UPC_ACarolina = String.Empty Then
                UPC_ACarolina = Mid(CodeB, 1, 1)
                For I As Integer = 2 To 6
                    Select Case Mid(CodeB, I, 1)
                        Case Is = "1"
                            UPC_ACarolina += "Q"
                        Case Is = "2"
                            UPC_ACarolina += "W"
                        Case Is = "3"
                            UPC_ACarolina += "E"
                        Case Is = "4"
                            UPC_ACarolina += "R"
                        Case Is = "5"
                            UPC_ACarolina += "T"
                        Case Is = "6"
                            UPC_ACarolina += "Y"
                        Case Is = "7"
                            UPC_ACarolina += "U"
                        Case Is = "8"
                            UPC_ACarolina += "I"
                        Case Is = "9"
                            UPC_ACarolina += "O"
                        Case Is = "0"
                            UPC_ACarolina += "P"
                    End Select
                Next
                UPC_ACarolina += "-"
                For I As Integer = 7 To 11
                    Select Case Mid(CodeB, I, 1)
                        Case Is = "1"
                            UPC_ACarolina += "A"
                        Case Is = "2"
                            UPC_ACarolina += "S"
                        Case Is = "3"
                            UPC_ACarolina += "D"
                        Case Is = "4"
                            UPC_ACarolina += "F"
                        Case Is = "5"
                            UPC_ACarolina += "G"
                        Case Is = "6"
                            UPC_ACarolina += "H"
                        Case Is = "7"
                            UPC_ACarolina += "J"
                        Case Is = "8"
                            UPC_ACarolina += "K"
                        Case Is = "9"
                            UPC_ACarolina += "L"
                        Case Is = "0"
                            UPC_ACarolina += ";"
                    End Select
                Next

                Dim CheckDigit As String = String.Empty
                If Len(CodeB) = 12 Then
                    CheckDigit = Mid(CodeB, 12, 2)
                Else
                    Dim Racine As String = Mid(CodeB, 1, 11)
                    Dim Facteur As Integer = 3
                    Dim Total As Integer = 0
                    For I As Integer = Len(Racine) To 1 Step -1
                        Total = Total + Mid(Racine, I, 1) * Facteur
                        Facteur = 4 - Facteur
                    Next I
                    CheckDigit = 10 - IIf(Total Mod 10 <> 0, Total Mod 10, 10)
                End If
                Select Case CheckDigit
                    Case Is = "1"
                        UPC_ACarolina += "Z"
                    Case Is = "2"
                        UPC_ACarolina += "X"
                    Case Is = "3"
                        UPC_ACarolina += "C"
                    Case Is = "4"
                        UPC_ACarolina += "V"
                    Case Is = "5"
                        UPC_ACarolina += "B"
                    Case Is = "6"
                        UPC_ACarolina += "N"
                    Case Is = "7"
                        UPC_ACarolina += "M"
                    Case Is = "8"
                        UPC_ACarolina += ","
                    Case Is = "9"
                        UPC_ACarolina += "."
                    Case Is = "0"
                        UPC_ACarolina += "/"
                End Select
            End If
        Catch ex As Exception

        End Try
    End Function



End Class
